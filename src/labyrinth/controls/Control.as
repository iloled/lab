package labyrinth.controls
{
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import labyrinth.level.Level;
	
	import labyrinth.level.Grid;
	import labyrinth.level.Player;
	import labyrinth.level.Tile;
	import labyrinth.events.GameEvent;

	public class Control
	{
		private var stage : Stage;
		private var container : Sprite;
		private var player : Player;
		private var tiles : Array;
		private var grid : Grid;
		public var enabled : Boolean;
		private var level : Level;
		
		public function Control( stage : Stage , level : Level )
		{
			this.stage = stage;
			this.level = level;
			initLevel();
			enabled = true;
		}
		
		public function initLevel() : void
		{
			this.container = level.getSprite();
			this.player = level.getPlayer();
			this.grid = level.grid;
		}
		
		public function init(): void
		{
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		}
		
		public function removeListener()
		{
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		}
		
		private function onEnterFrame( e : Event ) : void
		{
			
		}

		private function onKeyDown( e :KeyboardEvent) : void
		{
			if (!enabled)
				return;
				
			if (e.keyCode == Keyboard.LEFT) 
			{
				if( grid.canGoLeft(player.x, player.y ) )
				{
					container.x += Tile.TILE_SIZE;
					player.moveLeft();
					
					onMove();
				}
				
				else if ( grid.canGoLeftWithKeys(player.x, player.y,level.getKeys()))
				{
					level.openLeftDoor(player.x , player.y);
					container.x += Tile.TILE_SIZE;
					player.moveLeft();
					
					onMove();
				}
			}
			
			if (e.keyCode == Keyboard.T ) 
			{
				level.tryPutTorch( player.x , player.y);
			}

			
			if (e.keyCode == Keyboard.RIGHT) 
			{ 
				if( grid.canGoRight(player.x, player.y ))
				{
					container.x -= Tile.TILE_SIZE;
					player.moveRight();
					onMove();
				}
				
				else if ( grid.canGoRightWithKeys(player.x, player.y,level.getKeys()))
				{
					level.openRightDoor(player.x , player.y);
					container.x -= Tile.TILE_SIZE;
					player.moveRight();
					onMove();
				}
			
			}
			
			/*
			if (e.keyCode == Keyboard.S) 
			{ 
				stage.dispatchEvent(new Event(GameEvent.TIME_OUT));
				
			}*/
			
			if (e.keyCode == Keyboard.UP )
			{
				if( grid.canGoTop(player.x, player.y ) )
				{
					container.y += Tile.TILE_SIZE;
					player.moveTop();
					onMove();
				}
				
				else if ( grid.canGoTopWithKeys(player.x, player.y,level.getKeys()))
				{
					level.openTopDoor(player.x , player.y);
					container.y += Tile.TILE_SIZE;
					player.moveTop();
					onMove();
				}
			}
			
			if (e.keyCode == Keyboard.DOWN )
			{
				if( grid.canGoDown(player.x, player.y ) )
				{
					container.y -= Tile.TILE_SIZE;
					player.moveBottom();
					
					onMove();
				}
				
				else if ( grid.canGoBottomWithKeys(player.x, player.y,level.getKeys()))
				{
					level.openBottomDoor(player.x , player.y);
					
					container.y -= Tile.TILE_SIZE;
					player.moveBottom();
					
					onMove();
				}
			}
		}
		
		private function onMove() : void
		{
			testExit();
			grid.getItemOnTile(player.x , player.y , this.level );
			grid.testAndTriggerEvent(player.x , player.y , this.level);
		}
		
		private function testExit() : void
		{
			if (grid.isExit(player.x, player.y))
			{
				stage.dispatchEvent(new Event(GameEvent.STAGE_CLEAR));
				
			}
		}
	}
}