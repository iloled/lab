package labyrinth.mainscreen
{
	import flash.display.Bitmap;
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.BlurFilter;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.filters.GradientGlowFilter;
	import flash.text.TextField;
	
	import labyrinth.level.data.LevelGenerator;
	import labyrinth.events.GameEvent;
	import labyrinth.level.Grid;
	import labyrinth.utils.DisplayUtils;
	import labyrinth.utils.ImageUtils;

	public class MainMenu extends Sprite
	{
		public function MainMenu()
		{
			var background : Sprite = new Sprite;
			
			var grid :Grid = new Grid(30,30,background);
			grid.initRandomGrid(20);
			grid.displayMaze();
			background.alpha = 0.5;
			background.filters = [ new BlurFilter(7,7,2) ];
			
			addChild(background);
			
			var t : ImageUtils;
			
			
			var title : TextField;
			title = DisplayUtils.createTextWithDefaultFormat("MAZE ESCAPE",40);
			title.width = 300;
			title.x = 250;
			title.y = 100;
			addChild(title);
			
			var torch : Bitmap = ImageUtils.getTorchIcon();
			torch.y = 110;
			torch.x = 200;
			addChild(torch);
			
			/*
			var glow : GradientGlowFilter = new GradientGlowFilter();
			
			glow.color = 0xFFFF33 
			glow.alpha = 0.6;
			glow.blurX = 20;
			glow.blurY = 20;
			glow.strength = 10;
			glow.quality = BitmapFilterQuality.HIGH;*/
			var dropShadow: DropShadowFilter = new DropShadowFilter();
			dropShadow.distance = 0;
			dropShadow.angle = 45;
			dropShadow.color = 0xFFFF33;
			dropShadow.alpha = 0.5;
			dropShadow.blurX = 10;
			dropShadow.blurY = 10;
			dropShadow.strength = 10;
			dropShadow.quality = 15;
			dropShadow.inner = false;
			dropShadow.knockout = false;
			dropShadow.hideObject = false;
			var glow:GlowFilter = new GlowFilter(0xFFFF33, 0.8, 30, 30, 1, 1);
			
			var light : Sprite = new Sprite();
			light.y = 121;
			light.x = 211;
			//light.graphics.beginGradientFill(GradientType.RADIAL, [0xFFFF33, 0xFFFFFF], [1,0.4],[125, 255]
			//,null,"pad","rgb",20);
			light.graphics.beginFill(0xFFFF33,0.4);
			light.graphics.drawCircle(0, 0,25);
			light.graphics.endFill();
			light.filters = [dropShadow];
			addChild(light);
			
			var start : TextField = DisplayUtils.createTextWithDefaultFormat("START",40);
			start.width=150;
			start.x = 310;
			start.y = 200;
			addChild(start);
			start.addEventListener(MouseEvent.CLICK, 
				function (): void{ 
					stage.dispatchEvent(new Event(GameEvent.GAME_START))
				});
			/*
			var credit : TextField = DisplayUtils.createTextWithDefaultFormat("CREDITS",40);
			credit.width=150;
			credit.x = 295;
			credit.y = 250;
			addChild(credit);*/
		}
	}
}