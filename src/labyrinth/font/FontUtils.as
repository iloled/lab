package labyrinth.font
{
	public class FontUtils
	{
		[Embed(source="Maze.ttf", fontName = "maze", fontWeight = "bold",  mimeType="application/x-font-truetype", embedAsCFF="false")]
		private static var maze:Class;
		
		[Embed(source="Square.ttf", fontName = "square", fontWeight = "bold",  mimeType="application/x-font-truetype", embedAsCFF="false")]
		private static var square:Class;
		
		public function FontUtils()
		{
		}
	}
}