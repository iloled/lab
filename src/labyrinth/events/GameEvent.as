package labyrinth.events 
{
	/**
	 * ...
	 * @author pro
	 */
	public class GameEvent 
	{
		
		public static var STAGE_CLEAR : String = "CLEAR";
		public static var TIME_OUT : String = "TIME_OUT";
		public static var GAME_START : String = "GAME_START";
		public static var CENTER_NEEDED : String = "CENTER_NEEDED";
		public static var WARP : String = "WARP";
		
		
		public function GameEvent() 
		{
			
		}
		
	}

}