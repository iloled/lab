package labyrinth.utils
{
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.display.Stage;

	public class PopupManager
	{
		public function PopupManager()
		{
		}
		
		public static function createAndDisplayPopup ( container : DisplayObjectContainer , width : int , height : int ) : Sprite
		{
			var sprite : Sprite = new Sprite();
			sprite.graphics.clear();
			sprite.graphics.beginFill( 0x000000);
			sprite.graphics.drawRect(0, 0, width , height);
			sprite.graphics.endFill();
			sprite.graphics.lineStyle(5,0xffffff);
			sprite.graphics.drawRect(0,0,width,height);
			
			sprite.x = (container.mask.width/2)  - (width / 2);
			sprite.y = (container.mask.height/2)  - (height / 2) ;
			container.addChild(sprite);
			
			return sprite;
		}
	}
}