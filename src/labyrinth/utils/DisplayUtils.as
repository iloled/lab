package labyrinth.utils
{
	import flash.display.CapsStyle;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.filters.BlurFilter;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;

	public class DisplayUtils
	{
		public function DisplayUtils()
		{
		}
		
	
		
		public static function getDefaultTextFormat(size : int = 18, center : Boolean = false) : TextFormat
		{
			var format : TextFormat = new TextFormat(); 
			format.font = "square";
			format.color = 0xFFFFFF; 
			format.size = size;
			if (center)
				format.align = "center";
			return format;
			
		}
		
		public static function createTextWithDefaultFormat( text : String, size : int = 18, center : Boolean = false ) : TextField
		{
			var label : TextField  = new TextField;
			label.embedFonts = true;
			label.text = text;
			label.setTextFormat(DisplayUtils.getDefaultTextFormat(size,center));
			return label;
		}
		
		public static function createTextWithFont( 
			text : String , font : String , size : uint , color : uint = 0xffffff , center : Boolean = false) : TextField
		{
			var label : TextField  = new TextField;
			label.htmlText = "<strong>" + text + "</strong>";;
			label.embedFonts = true;
			var format : TextFormat = new TextFormat(); 
			format.color = color; 
			format.size = size;
			format.font = font;
			format.bold = true;
			if( center ) 
			{
				format.align = "center";
			}
			label.setTextFormat(format);
			return label;
		}
		
		public static function createMultiLineTextField( text : String) : TextField
		{
			var tf : TextField = new TextField
			tf.multiline = true;
			tf.wordWrap = true;
			return tf;
		}
		
		public static function createTextInput( 
			width : int , height : int , defaultText : String = "" , color : uint = 0xffffff) : TextField
		{
			var input : TextField = new TextField;
			input.type = TextFieldType.INPUT;
			input.background = true;
			input.backgroundColor = color;
			input.height = height;
			input.text = defaultText;
			input.width = width;
			return input;
		}
		
		public static function createButton(text : String , 
											backgroundColor : uint = 0xA9A9A9 , 
											textColor : uint = 0xFFFFFF,
											width : int = 50,
											height : int = 20,
											fontSize : int = 18) : Sprite
		{
				var button : Sprite = new Sprite;
				var t : TextField = DisplayUtils.createTextWithFont(text, 'square', fontSize, textColor,true);
				t.width = width;
				
				button.addChild(t);
				
				DisplayUtils.fillRectSprite(button, backgroundColor, width , height );
				
				
				return button;
		}
		
		public static function fillRectSprite(s : Sprite , color : uint , width : int , height : int , alpha : Number = 1) : void
		{
			s.graphics.clear();
			s.graphics.beginFill(color, alpha );
			s.graphics.drawRect(0,0, width , height );
			s.graphics.endFill();
			
		}
		
		public static function fillColor( s : Sprite , color : uint , alpha : Number = 1 ) : void
		{
			s.graphics.clear();
			s.graphics.beginFill(color, alpha );
			s.graphics.drawRect(0,0, s.width , s.height );
			s.graphics.endFill();
		}
		
		public static function drawBorder( s : Sprite , color : uint , thickness : int ) : void 
		{
			s.graphics.lineStyle(thickness, color ) ;
			s.graphics.drawRect( 0,0, s.width , s.height);
			
		}
		
		
		public static function setDoorLineStyle( s : Sprite , color : uint ) : void
		{
			s.graphics.lineStyle(4,color,1,true,"normal",CapsStyle.NONE);
		}
		
		
		public static function blurDisplayObject( d : DisplayObject ) : void
		{
			var blur : BlurFilter = new BlurFilter(6,6);
			d.filters = [ blur ];
		}
	}
}