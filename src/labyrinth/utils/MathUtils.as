package labyrinth.utils 
{
	/**
	 * ...
	 * @author pro
	 */
	public class MathUtils 
	{
		
		public function MathUtils() 
		{
			
		}
		
		
		public static function getRandomNumberInRange( min : int , max : int ) : int
		{
			return (Math.round(Math.random() * (max - min)) + min);
			
		}
		
		public static function getRoll100( threshold : int ) : Boolean
		{
			var roll : int = getRandomNumberInRange(0, 100);
			if ( roll < threshold ) 
				return true;
			return false;
		}
		
		public static function XYtoIndex( x : int , y : int , width : int ) : int
		{
			return y * width + x ;
		}
	}

}