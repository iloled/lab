package labyrinth.utils 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.ColorTransform;
	
	import labyrinth.level.Tile;

	/**
	 * ...
	 * @author pro
	 */
	public class ImageUtils 
	{
		[Embed(source = '../flashlight.png')]
		private static var visionClass:Class;
		
		[Embed(source = '../clock.png')]
		private static var clockClass:Class;
		
		[Embed(source = '../torch.png')]
		private static var torch:Class;
		
		[Embed(source = '../lit_torch.png')]
		private static var litTorch:Class;
		
		[Embed(source = '../exit.png')]
		private static var exit:Class;
		
		[Embed(source = '../start.png')]
		private static var start:Class;
		
		[Embed(source = '../write.png')]
		private static var write:Class;
		
		[Embed(source = '../warp.png')]
		private static var warp:Class;
		
		[Embed(source = '../cross.png')]
		private static var cross:Class;
		
		[Embed(source = '../lock.png')]
		private static var lock:Class;
		
		[Embed(source = '../key.png')]
		private static var key:Class;
		
		[Embed(source = '../step.png')]
		private static var step:Class;
		
		[Embed(source = '../settings.png')]
		private static var settings:Class;
		
		
		public function ImageUtils() 
		{
			
		}
		
		public static function getLockIcon() : Bitmap
		{
			return formatBitmapToIcon(new lock ());
		}
		
		public static function getVisionIcon() : Bitmap
		{
			return formatBitmapToIcon(new visionClass ());
		}
		
		public static function getTimeIcon() : Bitmap
		{
			return formatBitmapToIcon(new clockClass ());
		}
		
		public static function getTorchIcon() : Bitmap
		{
			return formatBitmapToIcon(new torch ());
		}
		
		public static function getLitTorchIcon() : Bitmap
		{
			return formatBitmapToIcon(new litTorch ());
		}
		
		public static function getExitIcon() : Bitmap
		{
			return formatBitmapToIcon(new exit() ) ;
		}
		
		public static function getStartIcon() : Bitmap
		{
			return formatBitmapToIcon(new start());
		}
		
		public static function getWriteIcon() : Bitmap
		{
			return formatBitmapToIcon(new write());
		}
		
		public static function getWarpIcon() : Bitmap
		{
			return formatBitmapToIcon(new warp());
		}
		
		public static function getCrossIcon() : Bitmap
		{
			return new cross();
		}
		
		public static function getStepIcon() : Bitmap
		{
			return formatBitmapToIcon(new step());
		}
		
		public static function getSettingsIcon() : Bitmap
		{
			return formatBitmapToIcon(new settings());
		}
		
		public static function getIconWithBackground(icon : Bitmap,  color : uint = 0x000000, width : int = 30, height : int = 30 ) : Sprite
		{
			var sprite : Sprite = new Sprite();
			
			sprite.graphics.beginFill(color, 1);
			sprite.graphics.drawRect(0,0,width,height);
			sprite.graphics.endFill();
			sprite.addChild(icon);
			
			icon.x = (width - icon.width) / 2;
			icon.y = (height - icon.height) / 2;
			return sprite;
		}
		
		private static function formatBitmapToIcon( b : Bitmap ) : Bitmap
		{
			var data : BitmapData = b.bitmapData;
			var b : Bitmap = new Bitmap(data);
			b.width = Tile.TILE_SIZE -2;
			b.height = Tile.TILE_SIZE -2;
			
			return b;
		}
		
		public static function changeBitmapSize( b : Bitmap , width : int , height : int ) : Bitmap
		{
			var transform : ColorTransform = b.transform.colorTransform;
			var data : BitmapData = b.bitmapData;
			var b : Bitmap = new Bitmap(data);
			b.width = width;
			b.height = height;
			b.transform.colorTransform = transform;
			return b;
		}
		
		public static function getKeyIcon(color : uint = 0) : Bitmap
		{
			var red : uint = (color & 0xFF0000 )>> 16;
			var blue : uint = color & 0x0000FF;
			var green : uint =( color & 0x00FF00) >> 8;
			var key : Bitmap = formatBitmapToIcon(new key());
			var transform : ColorTransform = new ColorTransform(1,1,1,1,red, green,blue);
			key.transform.colorTransform = transform;
			return key;
		}
		
		
		
	}

}