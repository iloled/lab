package labyrinth.level
{
	import flash.display.Sprite;
	
	// TODO REFACTOR : this class update the player position dot but also torch positions in the map
	// refactor somehow so torches are handled somewhere else
	// the main problem is about the draw function : it updates the level mask. this shouldn't be

	public class Player
	{
		public var x : int = 0;
		public var y : int = 0;
		
		public var sprite : Sprite;
		private var mask : Sprite;
		private var visionSprite : Sprite;
		
		private var vision : int = 2;
		public static var torchRadius : int = 3;
		
		private var torches : Array;
		
		public function Player( mask : Sprite, visionSprite : Sprite )
		{
			this.visionSprite = visionSprite
			this.mask = mask;
			sprite = new Sprite();
			torches = new Array();
		}
		
		public function resetTorches() : void
		{
			torches = new Array();
		}
		
		public function setPosition( x : int , y : int ) : void
		{
			this.x = x ;
			this.y = y;
			draw();
		}
		
		public function hide() : void 
		{
			sprite.visible = false;
		}
		
		public function show() : void 
		{
			sprite.visible = true;
		}
		
		public function moveRight(): void
		{
			this.x += 1;
			draw();
		}
		
		public function moveLeft(): void
		{
			this.x -= 1;
			draw();
		}
		
		public function moveTop() : void
		{
			this.y -= 1;
			draw();
		}
		
		public function moveBottom() : void
		{
			this.y += 1
			draw();
		}
		
		public function increaseVision() : void
		{
			this.vision ++;
			draw();
		}
		
		public function setVision( vision : int ) : void
		{
			this.vision = vision;
			draw();
		}
		
		public function moveTo( x : int , y : int ) : void
		{
			this.x = x ;
			this.y = y;
			//draw();
		}
		
		public function draw() : void
		{
			
			sprite.graphics.beginFill(0xFFFFFF, 0.9);
			sprite.graphics.drawCircle(0,0,Tile.TILE_SIZE/2 - 2);
			sprite.graphics.endFill();
			sprite.x = x * Tile.TILE_SIZE +  (Tile.TILE_SIZE / 2)-1;
			sprite.y = y * Tile.TILE_SIZE +  (Tile.TILE_SIZE / 2) - 1;
			
			drawMask();
			
		}
		
		public function addTorch( x : int , y : int ) : void
		{
			torches.push(
				[x * Tile.TILE_SIZE  +  (Tile.TILE_SIZE / 2)-1 ,
				y * Tile.TILE_SIZE  +  (Tile.TILE_SIZE / 2)-1]
			)
			;
			drawMask();
		}
		
		private function drawMask() : void
		{
			mask.graphics.clear();
			mask.graphics.beginFill( 0x000000, 1 );
			mask.graphics.drawCircle(sprite.x, sprite.y, Tile.TILE_SIZE * vision);
			mask.graphics.endFill();
			
			
			visionSprite.graphics.clear();
			visionSprite.graphics.beginFill( 0xffff99, 0.2 );
			visionSprite.graphics.drawCircle(sprite.x, sprite.y, Tile.TILE_SIZE * vision);
			visionSprite.graphics.endFill();
			
			/*
			mask.graphics.beginFill( 0x000000, 1 );
			mask.graphics.drawCircle(0, 0, Tile.TILE_SIZE * vision);
			mask.graphics.endFill();
			*/
			
			for each( var position : Array in torches ) 
			{
				mask.graphics.beginFill( 0x000000, 1 );
				mask.graphics.drawCircle(position[0] , position[1], Tile.TILE_SIZE * torchRadius);
				mask.graphics.endFill();
			}
		}
	}
}