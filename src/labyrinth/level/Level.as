package labyrinth.level 
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	
	import labyrinth.events.GameEvent;
	import labyrinth.level.Item.Item;
	import labyrinth.level.Item.KeyItem;
	import labyrinth.level.effect.WarpEffect;
	import labyrinth.utils.DisplayUtils;
	import labyrinth.utils.ImageUtils;
	import labyrinth.utils.MathUtils;

	
	public class Level 
	{
		
		private var gridWidth : int;
		private var gridHeight : int;
		private var player  : Player;
		private var levelSprite : Sprite;
		private var levelMask : Sprite;
		
		public var grid : Grid;
		private var inventory : Inventory;
		
		// time to escape the level
		
		private var baseTime : int;
		private var ms : int;
		private var timer : Timer;
		private var timerText : TextField;
		private var tileDatas : Array;
		private var startX : int;
		private var startY : int;
		private var stage : Stage;
		private var baseVision : int = 2;
		private var visionSprite : Sprite;
		public var message : String;
		public var levelName : String;
		public var retries : int = 0;
		// JSON 
		
		private var json : String;
		
		public static function createLevelFromJSON( json : String , stage : Stage ) : Level
		{
			var t : JSON;
			var data : Object = JSON.parse(json);
			var level : Level = new Level( data.width , data.height , stage );
			level.initLevelFromJSON(json);
			level.startX = data.start.x;
			level.startY = data.start.y;
			level.setBaseTime( data.baseTime);
			
			if( data.message !== undefined )
			{
				level.message = data.message;
			}
			
			if( data.levelName !== undefined )
			{
				level.levelName = data.levelName;
			}
			return level;
		}
		
		public function Level( width : int, height : int  , stage : Stage ) 
		{
			this.visionSprite = new Sprite;
			this.levelSprite = new Sprite;
			this.levelMask = new Sprite;
			this.levelSprite.addChild(levelMask);
			this.levelSprite.addChild(visionSprite);
			this.levelSprite.mask = levelMask;
			this.gridWidth = width;
			this.gridHeight = height;
			this.grid = new Grid(width, height , levelSprite );
			
			this.player = new Player(levelMask, visionSprite);
			this.player.setVision(baseVision);
			
			this.setBaseTime( 50000 );
			
			//trace("level width : " + gridWidth + " " + gridHeight);
			inventory = new Inventory();
			
			timerText = DisplayUtils.createTextWithDefaultFormat("",15);
			timerText.selectable = false;
			timerText.defaultTextFormat = DisplayUtils.getDefaultTextFormat();
			this.stage = stage;
			
		}
		
		
		
		public function initRandomLevel() : void
		{
			
			grid.initRandomGrid();
			
			
			startX = MathUtils.getRandomNumberInRange(0,gridWidth-1)
			startY = MathUtils.getRandomNumberInRange(0,gridHeight-1);
			this.baseTime = 60000;
			this.ms = baseTime;
			this.player.setPosition(startX, startY);
			
		}
		
		private function initLevelFromJSON( json : String ) : void
		{
			this.json = json;
			
			var data : Object = JSON.parse(json);
			startX = data.start.x;
			startY = data.start.y;
			grid.initGridFromJSON(json);
			this.baseTime = data.baseTime;
			this.ms = baseTime;
			this.player.setPosition(startX, startY);
		
			
		}
		
		public function reset() : void
		{
			this.retries ++;
			grid.clear();
			// remove torches
			//this.levelSprite.removeChildren();
			this.player.resetTorches();
			
			this.levelSprite.addChild(levelMask);
			this.levelSprite.mask = levelMask;
			this.levelSprite.addChild(visionSprite);

				
			this.player.setPosition(startX, startY);
			this.levelSprite.addChild(player.sprite);
			this.player.setVision(baseVision);
			this.setBaseTime(baseTime);
			inventory.clear();
			timer.stop();
			timer.removeEventListener(TimerEvent.TIMER, countdown) ;
			
			
		}
		
		public function setPlayerPosition( x : int, y : int ) : void
		{
		
			player.hide();
			player.moveTo(x , y );
			//this.levelSprite.x = player.x * Tile.TILE_SIZE;
			//this.levelSprite.y = player.y * Tile.TILE_SIZE;
			stage.dispatchEvent(new Event(GameEvent.CENTER_NEEDED));
			//player.draw();
		}
		
		public function start() : void 
		{
			inventory.clear();
			grid.displayMaze();
			if( baseTime >0)
			{
				timer = new Timer(50);
				timer.addEventListener(TimerEvent.TIMER, countdown , false , 0 , true);
				timer.start();
			}
			
			// TODO HAXX : initrandom level add tiles on top of the sprite
			// so to force the player to be in front we add it again!
			this.levelSprite.addChild(player.sprite);
		}
		
		public function stop() : void
		{
			if( timer != null ) 
			{
				timer.stop();
				timer = null;
			}
		}
		
		// TODO unused
		public function warpEffectOnCurrentTile() : void 
		{
			var tile : Tile = grid.getTile(this.player.x , this.player.y);
			var warpEffect : WarpEffect = new WarpEffect(tile);
			warpEffect.start()
		}
		
		public function addTime( ms : int ) : void
		{
			this.ms += ms ;
		}
		
		public function getRemainingSeconds() : Number 
		{
			return Math.floor(ms/1000);
		}

		
		public function addItem( item : Item ) : void 
		{
			inventory.addItem(item);
		}
		
		public function getPlayer() : Player
		{
			return player;
		}
		
		public function getSprite() : Sprite
		{
			return levelSprite;
		}
		
		public function getTimerText() : TextField
		{
			return this.timerText;
		}
		
		public function getInventoryDisplay() : Sprite
		{
			return inventory.getDisplay();
		}
		
		public function getGrid() : Grid
		{
			return grid;
		}
		
		public function useKey( color : uint ) : void
		{
			inventory.useKey(color);
		}
		
		public function openBottomDoor( x : int , y : int) : void 
		{
			var doorColor  : uint = grid.removeBottomDoor(x, y);
			inventory.useKey(doorColor);
		}
		
		public function openLeftDoor( x : int , y : int) : void 
		{
			var doorColor  : uint = grid.removeLeftDoor(x, y);
			inventory.useKey(doorColor);
		}
		
		public function openRightDoor( x : int , y : int) : void 
		{
			var doorColor  : uint = grid.removeRightDoor(x, y);
			inventory.useKey(doorColor);
		}
		
		public function openTopDoor( x : int , y : int) : void 
		{
			var doorColor  : uint = grid.removeTopDoor(x, y);
			inventory.useKey(doorColor);
		}
		
		public function getKeys() : Dictionary
		{
			return inventory.getKeys();
		}
		
		public function removeDoors( color : uint ) : void 
		{
			grid.removeDoors(color);
		}
		
		public function tryPutTorch( x : int , y : int ) : void
		{
			if ( inventory.hasTorchInInventory() ) 
			{
				if ( !grid.hasTorch( x , y ) )
				{
					this.useItem("TorchItem");
					
					// add torch overlay
					
					var sprite : Sprite = new Sprite();
					
					sprite.graphics.beginFill( 0xFFFF33, 0.3 );
					sprite.graphics.drawCircle(0, 0, Player.torchRadius * Tile.TILE_SIZE );
					sprite.graphics.endFill();
					
					var padding : int =  (Tile.TILE_SIZE / 2 - 1);
					sprite.x = x * Tile.TILE_SIZE + padding ;
					sprite.y = y *Tile.TILE_SIZE +  padding;
					
					levelSprite.addChild(sprite);
				}
			}
		}
		
		public function useItem( itemName : String ) : void
		{
			var item : Item = inventory.getItem(itemName);
			item.useItem(this);
			inventory.displayItems();
		}
		
		private function countdown( e : TimerEvent ) : void
		{
			ms -= 50;
			
			var msString : String = Math.floor((ms % 1000 )/10).toString();
			if( msString.length == 1 )
			{
				msString += "0";
			}
			
			// update timer display
			var m :Math;
			this.timerText.text = String( Math.floor(ms / 1000) + ":" +  msString );
			
			if ( ms <= 0 )
			{
				stage.dispatchEvent(new Event(GameEvent.TIME_OUT));
				timer.stop();
			}
		}
		
		private function setBaseTime( time : uint ) : void
		{
			this.baseTime = time;
			this.ms = time;
		}
	}

}