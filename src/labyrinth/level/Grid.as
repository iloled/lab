package labyrinth.level
{
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	
	import labyrinth.level.data.LevelGenerator;
	import labyrinth.level.Item.Item;
	import labyrinth.level.Item.ItemFactory;
	import labyrinth.level.Item.TimeItem;
	import labyrinth.level.Item.TorchItem;
	import labyrinth.level.Item.VisionItem;
	import labyrinth.level.event.EventFactory;
	import labyrinth.utils.MathUtils;
	
	public class Grid
	{
		
		public var width : int;
		public var height : int;
		private var sprite : Sprite;
		
		private var exitX : int ;
		private var exitY : int ;
		
		private var maze : Maze;
		private var tiles : Array;
		
		public function Grid( width : int , height : int, sprite : Sprite)
		{
			this.width = width;
			this.height = height;
			this.sprite = sprite;
			tiles = new Array;
		}
		
		public function clear() : void
		{
			sprite.removeChildren();
		}
		
		public function initRandomGrid(probability : int = 10) : void
		{
			var generator : LevelGenerator = new LevelGenerator;
			maze = generator.randomMaze(width,  height, probability);
			
		}
		
		public function initRecursiveBacktracker()
		{
			var generator : LevelGenerator = new LevelGenerator;
			maze = generator.recursiveBacktracker(width , height);
		}
		
		public function initGridFromJSON( json : String )  : void
		{
			var generator : LevelGenerator = new LevelGenerator;
			maze = generator.JSONtoMaze(json);
			maze.removeWallsUnderDoors();
		}
		
		public function displayMaze() : void
		{
			displayGridFromMaze();
		}
		
		private function displayGridFromMaze() : void
		{
			var cells : Vector.<Cell> = maze.getCells();
			tiles = new Array();
			for(var i : int = 0; i < cells.length ; i ++ )
			{
				var tile : Tile = cellToTile(cells[i]);
				sprite.addChild(tile);
				tiles.push(tile);
			}
		}
		
		private function cellToTile( cell : Cell ) : Tile
		{
			var tile : Tile = new Tile();
			tile.x = cell.xpos * Tile.TILE_SIZE;
			tile.y = cell.ypos * Tile.TILE_SIZE;
			tile.setWalls( cell.top , cell.right , cell.bottom , cell.left);
			
			tile.doorBottom = cell.doorBottom;
			tile.doorLeft = cell.doorLeft;
			tile.doorRight = cell.doorRight;
			tile.doorTop = cell.doorTop;
			tile.draw();
			
			if( cell.exit ) 
				tile.setExit();
			
			var t : ItemFactory;
			if ( cell.hasItem() ) 
				tile.addItem(ItemFactory.createItemByNumber(cell.item, cell.itemParameters));
			
			if ( cell.hasEvent() ) 
			{
				tile.addEvent(cell.event);
			}
			return tile;
		}
		
		public function canGoLeft( x : int , y : int ) : Boolean
		{
			if( x <= 0  )
				return false;
			
			var tile : Tile = getTile( x , y );
			var leftTile : Tile = getTile( x-1 , y );
			if ( tile.canGoLeft() && leftTile.canGoRight() ) 
				return true;
			
			return false;
		}
		
		public function canGoLeftWithKeys( x : int , y : int, keys : Dictionary ) : Boolean
		{
			if( x <= 0 )
				return false;
			
			var tile : Tile = getTile( x , y );
			var leftTile : Tile = getTile( x-1 , y );
			
			return tile.testLeftSideDoor(keys) || leftTile.testRightSideDoor(keys);
		}
		
		public function removeLeftDoor( x : int , y : int ) : uint 
		{
			var tile : Tile = getTile( x , y );
			var leftTile : Tile = getTile( x-1 , y );
			
			var color : uint = tile.doorLeft | leftTile.doorRight;
			tile.removeLeftDoor();
			leftTile.removeRightDoor();
			
			return color;
		}
		
		public function canGoRight( x : int , y : int ) : Boolean
		{
			if( x >= this.width -1 || y < 0 )
				return false;
			
			var tile : Tile = getTile( x , y );
			var rightTile : Tile = getTile( x+1 , y );
			if ( tile.canGoRight() && rightTile.canGoLeft() ) 
				return true;
			
			return false;
		}
		
		public function canGoRightWithKeys( x : int , y : int, keys : Dictionary ) : Boolean
		{
			if( x >= this.width -1 || y < 0 )
				return false;
			
			var tile : Tile = getTile( x , y );
			var rightTile : Tile = getTile( x+1 , y );
			
			return tile.testRightSideDoor(keys) || rightTile.testLeftSideDoor(keys);
		}
		
		public function removeRightDoor( x : int , y : int ) : uint 
		{
			var tile : Tile = getTile( x , y );
			var rightTile : Tile = getTile( x+1 , y );
			
			var color : uint = tile.doorRight | rightTile.doorLeft;
			tile.removeRightDoor();
			rightTile.removeLeftDoor();
			return color;
		}
		
		public function canGoTop( x : int , y : int ) : Boolean
		{
			if(  y <= 0 )
				return false;
			
			var tile : Tile = getTile( x , y );
			var topTile : Tile = getTile( x , y-1 );
			if ( tile.canGoTop() && topTile.canGoDown() ) 
				return true;
			
			return false;
		}
		
		public function canGoTopWithKeys( x : int , y : int, keys : Dictionary ) : Boolean
		{
			if( y <= 0 )
				return false;
			
			var tile : Tile = getTile( x , y );
			var topTile : Tile = getTile( x , y-1 );
			
			return tile.testTopSideDoor(keys) || topTile.testBottomSideDoor(keys);
		}
		
		public function removeTopDoor( x : int , y : int ) : uint 
		{
			var tile : Tile = getTile( x , y );
			var topTile : Tile = getTile( x , y-1 );
			
			var color : uint = tile.doorTop | topTile.doorBottom;
			tile.removeTopDoor();
			topTile.removeBottomDoor();
			return color;
		}
		
		
		public function canGoDown( x : int , y : int ) : Boolean
		{
			if( x <0 || y >= height-1 )
				return false;
			
			var tile : Tile = getTile( x , y );
			var bottomTile : Tile = getTile( x , y+1 );
			if ( tile.canGoDown() && bottomTile.canGoTop() ) 
			{
				return true;
			}
			
			return false;
		}
		
		public function canGoBottomWithKeys( x : int , y : int, keys : Dictionary ) : Boolean
		{
			if( x <0 || y >= height-1 )
				return false;
			
			var tile : Tile = getTile( x , y );
			var bottomTile : Tile = getTile( x , y+1 );
			
			return tile.testBottomSideDoor(keys) || bottomTile.testTopSideDoor(keys);
		}
		
		public function removeBottomDoor( x : int , y : int ) : uint 
		{
			var tile : Tile = getTile( x , y );
			var bottomTile : Tile = getTile( x , y+1 );
			
			var color : uint = tile.doorBottom | bottomTile.doorTop;
			tile.removeBottomDoor();
			bottomTile.removeTopDoor();
			return color;
		}
		
		public function removeDoors( color : uint ) : void 
		{
			for each ( var tile : Tile in tiles ) 
			{
				tile.removeDoors(color);
			}
		}
		
		public function getItemOnTile( x : int , y : int  , level : Level ) : void
		{
			var tile : Tile = getTile(x, y);
			if ( tile.hasItem() )
			{
				level.addItem(tile.getItem());
				tile.getItemAction(level);
				
			}
		}
		
		public function testAndTriggerEvent( x : int , y : int , level : Level) : void
		{
			var tile : Tile = getTile(x, y);
			
			if( tile.hasEvent() ) 
			{
				tile.triggerEvent(level);
			}
		}
		
		public function hasTorch( x : int , y : int ) : Boolean
		{
			return getTile(x, y).hasTorch;
		}
		
		public function putTorch( x : int , y : int ) : void
		{
			getTile(x, y).putTorch();
		}
		
		public function isExit( x : int , y : int ) : Boolean
		{
			return getTile(x, y).testExit();
		}
		
		public function getTile( x : int , y : int ) : Tile
		{
			
			return tiles[y * this.width + x];
		}
		
		private function getIndexFromPosition( x : int , y : int ) : int
		{
			return y * this.width + x;
		}
	}
}