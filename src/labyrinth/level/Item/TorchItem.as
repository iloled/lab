package labyrinth.level.Item
{
	import flash.display.Bitmap;
	
	import labyrinth.level.Level;
	import labyrinth.level.Player;
	import labyrinth.utils.ImageUtils;
	
	public class TorchItem implements Item
	{
		public function TorchItem()
		{
		}
		
		public static var count : int = 0;
		public function getBitmap():Bitmap
		{
			count ++;
			return ImageUtils.getTorchIcon();
		}
		
		public function getItemAction(level:Level):void
		{
		}
		
		public function useItem(level:Level):void
		{
			var player : Player = level.getPlayer();
			level.getGrid().putTorch( player.x , player.y );
			player.addTorch(player.x , player.y );
		}
	}
}