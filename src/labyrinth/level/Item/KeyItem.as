package labyrinth.level.Item
{
	import flash.display.Bitmap;
	
	import labyrinth.level.Level;
	import labyrinth.utils.ImageUtils;
	
	public class KeyItem implements Item
	{
		public var color : uint;
		
		public function KeyItem(color : uint)
		{
			this.color = color;
		}
		
		public function getBitmap():Bitmap
		{
			return ImageUtils.getKeyIcon(color);
		}
		
		public function getItemAction(level:Level):void
		{
		}
		
		public function useItem(level:Level):void
		{
			
		}
	}
}