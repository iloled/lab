package labyrinth.level.Item
{
	public class ItemFactory
	{
		public static var TIME : int = 1;
		public static var TORCH : int = 2;
		public static var VISION : int = 3;
		public static var KEY : int = 4;
		
		public function ItemFactory()
		{
		}
		
		public static function createItemByNumber( n : int , parameters : Object = null) : Item
		{
			switch(n)
			{
				case TIME:
					return new TimeItem();
					break;
				case TORCH:
					return new TorchItem();
					break;
				case VISION:
					return new VisionItem();
					break;
				case KEY:
					return new KeyItem(parameters.color);
					break;
			}
			
			return null;
		}
	}
}