package labyrinth.level.Item 
{
	import flash.display.Bitmap;
	import labyrinth.level.Level;
	
	/**
	 * ...
	 * @author pro
	 */
	public interface Item 
	{
		
		function getBitmap() : Bitmap;
		function getItemAction( level : Level ) : void;
		function useItem( level : Level ) : void;
	}
	
}