package labyrinth.level.Item 
{
	import labyrinth.level.Level;
	import flash.display.Bitmap;
	import labyrinth.utils.ImageUtils;
	/**
	 * ...
	 * @author pro
	 */
	public class VisionItem implements Item 
	{
		
		public function VisionItem() 
		{
			
		}
		
		/* INTERFACE labyrinth.level.Item.Item */
		
		public function getBitmap():Bitmap 
		{
			return ImageUtils.getVisionIcon();
		}
		
		public function getItemAction(level:Level) : void
		{
			useItem(level);
		}
		
		public function useItem(level:Level):void 
		{
			level.getPlayer().increaseVision();
		}
		
	}

}