package labyrinth.level.Item
{
	import flash.display.Bitmap;
	
	import labyrinth.level.Level;
	import labyrinth.utils.ImageUtils;
	
	public class TimeItem implements Item
	{
		public function TimeItem()
		{
		}
		
		public function getBitmap():Bitmap
		{
			return ImageUtils.getTimeIcon();
		}
		
		public function getItemAction(level:Level):void
		{
			level.addTime(10000);
		}
		
		public function useItem(level:Level):void
		{
		}
	}
}