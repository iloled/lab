package labyrinth.level 
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	
	import labyrinth.level.Item.Item;
	import labyrinth.level.Item.KeyItem;
	import labyrinth.utils.DisplayUtils;
	import labyrinth.utils.ImageUtils;

	/**
	 * ...
	 * @author pro
	 */
	public class Inventory 
	{
		
		private var items : Dictionary;
		private var display : Sprite;
		private var keys : Dictionary ;
		
		public function Inventory() 
		{
			items = new Dictionary(true);
			display = new Sprite();
			keys = new Dictionary(true);
		}
		
		public function clear() : void
		{
			items = new Dictionary(true);
			displayItems();
		}
		
		public function addItem( item : Item ) : void
		{
			if ( item is KeyItem )
			{
				addKey(item as KeyItem);
			}
			else
			{
				var className : String = flash.utils.getQualifiedClassName(item);
				className = className.slice(className.lastIndexOf("::") + 2);
				if ( items [className] == undefined ) 
					items[className] = new Array();
				
				items[className].push(item);
				
			}
		
			displayItems();
		}
		
		public function getKeys() : Dictionary
		{
			return keys;
		}
		
		public function useKey( color : uint ) : void 
		{
			if ( keys[color] !== undefined )
			{
				var keyArray : Array = keys[color];
				keyArray.pop();
				
				if( keyArray.length == 0 )
					delete  keys[color]
					
				displayItems();
			}
		}
		
		private function addKey( key : KeyItem ) : void 
		{
			if( keys[key.color] == undefined )
			{
				keys[key.color] = new Array();
			}
			
			keys[key.color].push(key);
		}
		
		public function getDisplay() : Sprite
		{
			return display;
		}
		
		public function hasTorchInInventory() : Boolean
		{
			trace(items["TorchItem"] != undefined);
			trace(items["TorchItem"]);
			
			return items["TorchItem"] != undefined;
		}
		
		public function getItem( itemName : String ) : Item
		{
			if( items[itemName] == undefined)
				return null;
			
			var item : Item = (items[itemName] as Array).pop();
			if( (items[itemName] as Array).length == 0 )
			{
				delete items[itemName];
			}
			return item;
			
		}
		
		public function displayItems() : void
		{
			var position : int = 40;
			display.removeChildren();
			
			for each (var itemArray : Array in items) {
				var sprite : Sprite = this.getItemDisplay(itemArray);
				sprite.y = position;
				position += 40;
				display.addChild(sprite);
			}
			
			for (var keyColor:Object in keys) {
				var keyArray : Array = keys[keyColor];
				var sprite : Sprite = this.getItemDisplay( keyArray);
				sprite.y = position;
				position += 40;
				display.addChild(sprite);
			}
		}
		
		private function getItemDisplay( items : Array ) : Sprite 
		{
			var sprite : Sprite = new Sprite();
			var item : Item = items[0];
			var bitmap : Bitmap = ImageUtils.changeBitmapSize(item.getBitmap(), 20,20);
			bitmap.x = 12
			sprite.addChild(bitmap);
			
			var text : TextField = DisplayUtils.createTextWithDefaultFormat("X "+ items.length);

			text.x = 40;
		
			sprite.addChild(text);
			return sprite;
		}
		
	}
	
	

}