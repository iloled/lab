package labyrinth.level.event
{
	import flash.display.Bitmap;
	import flash.geom.ColorTransform;
	
	import labyrinth.level.Level;
	import labyrinth.utils.ImageUtils;
	
	public class SwitchEvent implements IMazeEvent
	{
		public var color : uint;
		public function SwitchEvent(color : uint)
		{
			this.color = color;
		}
		
		public function triggerEvent(level:Level):void
		{
			level.removeDoors(color);
		}
		
		public function getBitmap():Bitmap
		{
			var red : uint = (color & 0xFF0000 )>> 16;
			var blue : uint = color & 0x0000FF;
			var green : uint =( color & 0x00FF00) >> 8;
			var step : Bitmap = ImageUtils.getStepIcon();
			var transform : ColorTransform = new ColorTransform(1,1,1,1,red, green,blue);
			step.transform.colorTransform = transform;
			return step;
			return ImageUtils.getStepIcon();
		}
	}
}