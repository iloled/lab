package labyrinth.level.event
{
	import flash.display.Bitmap;
	
	import labyrinth.level.Level;

	public interface IMazeEvent
	{
		function triggerEvent( level : Level ) : void;
		function getBitmap() : Bitmap;
	}
}