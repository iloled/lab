package labyrinth.level.event
{
	public class EventFactory
	{
		public static var WARP : int = 1;
		public static var SWITCH : int = 2;
		
		public function EventFactory()
		{
		}
		
		public static function createEventByNumber( n : int , parameters : Object = null) : IMazeEvent
		{
			switch(n)
			{
				
				case SWITCH:
					return new SwitchEvent(parameters.color);
					break;
			}
			
			return null;
		}

	}
}