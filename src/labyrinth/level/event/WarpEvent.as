package labyrinth.level.event
{
	import flash.display.Bitmap;
	
	import labyrinth.level.Level;
	import labyrinth.utils.DisplayUtils;
	import labyrinth.utils.ImageUtils;
	
	public class WarpEvent implements IMazeEvent
	{
		
		public var sourceX : int;
		public var sourceY : int;
		
		public var destX : int;
		public var destY : int ;
		public function WarpEvent( sourceX : int , sourceY : int , destX : int , destY : int)
		{
			this.sourceX = sourceX;
			this.sourceY = sourceY;
			this.destX = destX;
			this.destY = destY;
		}
		
		public function triggerEvent(level:Level):void
		{
			level.warpEffectOnCurrentTile();
			level.setPlayerPosition(destX, destY);
		}
		
		public function getBitmap() : Bitmap
		{
			return ImageUtils.getWarpIcon();
		}
	}
}