package labyrinth.level.data
{
	import flash.display.Stage;
	
	import labyrinth.level.Cell;
	import labyrinth.level.Item.ItemFactory;
	import labyrinth.level.Level;
	import labyrinth.level.Maze;
	import labyrinth.level.Tile;
	import labyrinth.level.event.EventFactory;
	import labyrinth.level.event.IMazeEvent;
	import labyrinth.level.event.WarpEvent;
	import labyrinth.utils.MathUtils;

	public class LevelGenerator
	{
		public function LevelGenerator()
		{
		}
		
		[Embed(source="level1.json",mimeType="application/octet-stream")]
		private static var l1:Class;
		
		[Embed(source="level2.json",mimeType="application/octet-stream")]
		private static var l2:Class;
		
		[Embed(source="level3.json",mimeType="application/octet-stream")]
		private static var l3:Class;
		
		[Embed(source="level4.json",mimeType="application/octet-stream")]
		private static var l4:Class;
		
		[Embed(source="level5.json",mimeType="application/octet-stream")]
		private static var l5:Class;
		
		[Embed(source="level6.json",mimeType="application/octet-stream")]
		private static var l6:Class;
		
		[Embed(source="level7.json",mimeType="application/octet-stream")]
		private static var l7:Class;
		
		[Embed(source="level8.json",mimeType="application/octet-stream")]
		private static var l8:Class;
		
		public static function getJSONLevels( stage : Stage) : Array
		{
			var levels : Array = new Array();
			
			createAndPushJSONLevel( new l1() , levels , stage );
			createAndPushJSONLevel( new l2() , levels , stage );
			createAndPushJSONLevel( new l3() , levels , stage );
			createAndPushJSONLevel( new l8() , levels , stage );
			createAndPushJSONLevel( new l4() , levels , stage );
			createAndPushJSONLevel( new l7() , levels , stage );
			createAndPushJSONLevel( new l6() , levels , stage );
			createAndPushJSONLevel( new l5() , levels , stage );
			
			
			return levels;
		}
		
		private static function createAndPushJSONLevel( json : String , levels : Array , stage : Stage ) : void 
		{
			var customLevel : Level = Level.createLevelFromJSON( json , stage ) ;
			levels.push(customLevel);
		}
		
		public function JSONtoMaze( json : String ) : Maze
		{
			var data : Object = JSON.parse(json);
			var width : int = data.width;
			var height : int = data.height;
			var maze : Maze = new Maze(width , height);
			maze.setCells(getCellsFromJSONData(data));
			return maze;
		}
		
		private function getCellsFromJSONData( data : Object ) : Vector.<Cell>
		{
			var datas : Array = data.tiles;
			var cells : Vector.<Cell> = new Vector.<Cell>();
			for(var i : int = 0; i < datas.length ; i ++ )
			{
				var d : Object = datas[i];
				var cell : Cell = new Cell();

				cell.setWalls(d.top, d.right, d.bottom, d.left);
				cell.xpos = d.x ;
				cell.ypos = d.y;
				cell.index = i;
				cell.doorBottom = d.doorBottom;
				cell.doorLeft = d.doorLeft;
				cell.doorRight = d.doorRight;
				cell.doorTop = d.doorTop;
				
				cells.push(cell);
				
				
				if(d.exit)
					cell.exit = true;
				if(d.item != -1)
				{
					cell.item = d.item;
					cell.itemParameters = d.itemParameters;
				}
				
				if( d.event != -1)
				{
					var t : EventFactory;
					cell.event = EventFactory.createEventByNumber(d.event, d.eventParameter);
				}
					
				
			}
			
			for each ( var warp : Object in data.warps ) 
			{
				var warpEvent : IMazeEvent = new WarpEvent(warp.sourceX, warp.sourceY, warp.destX , warp.destY)
				var cell : Cell = cells[(warp.sourceY * data.width + warp.sourceX)];
				cell.event = warpEvent;
			}
			
			return cells;
		}
		
		public function randomMaze(  width : int , height : int , probability : int = 10 ) : Maze
		{
			var maze : Maze = new Maze(width,height);
			var cells : Vector.<Cell> = new Vector.<Cell>();
			
			var number : int = 0;
			for(var i : int =  0; i < height ; i ++ )
			{
				for ( var j : int = 0; j < width ; j++ )
				{
					var t : MathUtils;
					var top : Boolean = MathUtils.getRoll100(probability);
					var right : Boolean = MathUtils.getRoll100(probability);
					var bottom : Boolean = MathUtils.getRoll100(probability);
					var left : Boolean = MathUtils.getRoll100(probability);
					
					var cell : Cell = new Cell();
					cell.setWalls(  top, right,bottom,left );
					cell.xpos = j;
					cell.ypos = i;
					cell.index = i * width + j;
					
					cells.push(cell);
					
					number ++;
				}
				
			}
			
			
			
			// set random exit
			
			var exitX : int = MathUtils.getRandomNumberInRange(0, width - 1);
			var exitY : int = MathUtils.getRandomNumberInRange(0, height - 1);
			
			var index : int =  MathUtils.XYtoIndex(exitX, exitY, width);
			cells[index].exit = true;
			
			maze.setCells(cells);
			
			// put random vision just because
			
			for (var i : int = 0; i < 2 ; i++ )
			{
				maze.addItemOnRandomEmptyCell(ItemFactory.VISION);
			}
			
			// put random time just because 
			for (var i : int = 0; i < 2 ; i++ )
			{
				maze.addItemOnRandomEmptyCell(ItemFactory.TIME);
			}
			
			// put random torch just because
			
			for (var i : int = 0; i < 2 ; i++ )
			{
				maze.addItemOnRandomEmptyCell(ItemFactory.TORCH);
			}
			
			// put 2 random warps
			
			for (var i : int = 0; i < 2 ; i++ )
			{
				maze.addEventWarpEventOnEmptyCells();
			}
			
			return maze;
		}
		
		
		
		public function recursiveBacktracker( width : int , height : int ) : Maze
		{
			var maze : Maze = new Maze(width,height);
			var cells : Vector.<Cell> = new Vector.<Cell>();
			var stack : Array = new Array();
			
			for ( var i : int = 0 ; i < width * height ; i ++)
			{
				var c : Cell = new Cell;
				c.setAllWalls();
				c.index = i;
				cells.push(c);
				
			}
			
			var currentCell : Cell = cells[0];
			var currentIndex : int = 0;
			currentCell.start = true;
			currentCell.visited = true;
			var turn : int = 0;
			while(true)
			{
				var unvisitedNeighbour : Array = this.getUnvisitedNeighbourCells(cells, currentIndex, width , height);
				
				if( stack.length == 0 && unvisitedNeighbour.length == 0)
					break
					
				if( unvisitedNeighbour.length == 0 )
				{
					currentCell = stack.pop();
					currentIndex = currentCell.index;
				}
				else
				{
					stack.push(currentCell);
					var m : MathUtils;
					var random : int = MathUtils.getRandomNumberInRange(0, unvisitedNeighbour.length -1);
	
					removeWallBetweenCells( currentCell , unvisitedNeighbour[random], width);
					currentCell = unvisitedNeighbour[random];
					currentCell.visited = true;
					currentIndex = currentCell.index;
					
					// remove walls between cell
				}
				turn ++;
			}
			maze.setCells(cells);
			return maze;
		}
		
		private function removeWallBetweenCells( c1 : Cell , c2 : Cell , width :int  ) : void
		{

			if ( c1.index + 1 == c2.index )
			{
				c1.right = false;
				c2.left = false;
				return;
			}
			
			if ( c1.index - 1 == c2.index )
			{
				c1.left = false;
				c2.right = false;
				return;
			}
			
			if ( c1.index + width == c2.index )
			{
				c1.bottom = false;
				c2.top = false;
				return;
			}
			
			if ( c1.index - width == c2.index )
			{
				c1.top = false;
				c2.bottom = false;
				return;
			}
			
			
		}
		
		private function getTopCell ( cells : Vector.<Cell> , currentIndex : int , width : int ) : Cell
		{
			if( currentIndex < width )
				return null
			
			return cells[currentIndex - width];
		}
		
		private function getBottomCell ( cells : Vector.<Cell> , currentIndex : int , width : int , height : int ) : Cell
		{
			if( currentIndex >= height * (width-1) )
				return null
			
			return cells[currentIndex + width];
		}
		
		private function getLeftCell ( cells : Vector.<Cell> , currentIndex : int , width : int ) : Cell
		{
			if( currentIndex % width == 0 )
				return null
			
			return cells[currentIndex - 1];
		}
		
		private function getRightCell ( cells : Vector.<Cell> , currentIndex : int , width : int, height : int ) : Cell
		{
			if( (currentIndex + 1) % width == 0 || currentIndex +1 == width * height)
				return null
			
			return cells[currentIndex + 1 ];
		}
		
		private function getUnvisitedNeighbourCells( cells : Vector.<Cell> , index : int, width : int , height : int) : Array
		{
			var result : Array = new Array;
			var rightCell : Cell = getRightCell(cells, index, width , height );
			
			if ( rightCell != null && !rightCell.visited ) 
			{
				result.push(rightCell);
			}
			
		
			
			var leftCell : Cell = getLeftCell(cells, index , width );
			if ( leftCell != null && !leftCell.visited ) 
			{
				result.push(leftCell);
			}
			
			var topCell : Cell = getTopCell(cells, index , width );
			if ( topCell != null && !topCell.visited ) 
			{
				result.push(topCell);
			}
			
			var bottomCell : Cell = getBottomCell(cells, index , width, height );
			if ( bottomCell != null && !bottomCell.visited ) 
			{
				result.push(bottomCell);
			}
			
			
				
			return result;
		}
	}
}