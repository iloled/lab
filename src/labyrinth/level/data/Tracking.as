package labyrinth.level.data
{
	import flash.external.ExternalInterface;

	public class Tracking
	{
		public function Tracking()
		{
		}
		
		public static function trackStart() : void 
		{
			ExternalInterface.call("start");
		}
		
		public static function trackPlay( stageNumber : int ) : void 
		{
			ExternalInterface.call("play", stageNumber);
		}
		
		public static function trackRetry( stageNumber : int ) : void 
		{
			ExternalInterface.call("retry", stageNumber);
		}
		
		public static function trackEndGame( retries : int , extraTime : int , score : int ) : void 
		{
			ExternalInterface.call("score", retries,extraTime, score);
		}
		
		public static function trackComplete( stageNumber : int , retries : int , extraTime : int) : void 
		{
			ExternalInterface.call("complete", stageNumber , retries , extraTime);
		}
	}
}