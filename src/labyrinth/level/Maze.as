package labyrinth.level
{
	import labyrinth.level.Item.ItemFactory;
	import labyrinth.level.event.IMazeEvent;
	import labyrinth.level.event.WarpEvent;
	import labyrinth.utils.MathUtils;

	public class Maze
	{
		
		private var cells : Vector.<Cell>;
		private var width : int;
		private var height : int;
		
		public function Maze( width : int , height : int )
		{
			this.width = width;
			this.height = height;
		}
		
		public function setCells( cells : Vector.<Cell> ) : void
		{
			this.cells = cells;
		}
		
		public function getCells() : Vector.<Cell>
		{
			return cells;
		}
		
		public function XYtoIndex( x : int , y : int ) : int 
		{
			return y * width + x ;
		}
		
		public function getRightCell( x : int , y : int ) : Cell
		{
			if( x >= this.width -1 || y < 0 )
				return null;
			
			return cells[XYtoIndex( x+1 , y )];
		}
		
		public function getLeftCell( x : int , y : int ) : Cell
		{
			if( x <= 0  )
				return null;
			
			return cells[XYtoIndex( x-1 , y )];
		}
		
		public function getTopCell( x : int , y : int ) : Cell
		{
			if( y <= 0 )
				return null ;
			
			return cells[XYtoIndex( x , y-1  )];
		}
		
		public function getBottomCell( x : int , y : int ) : Cell
		{
			if( y >= height-1 )
				return null ;
			
			return cells[XYtoIndex( x , y+1  )];
		}
		
		
		public function removeWallsUnderDoors() : void 
		{
			for each ( var cell : Cell in cells)
			{
				
				if( cell.hasDoorTop() )
				{
					cell.removeTopWall();
					var topCell : Cell = getTopCell( cell.xpos, cell.ypos );
					if ( topCell != null)
					{
						topCell.removeBottomWall();
					}
				}
				
				if( cell.hasDoorRight())
				{
					var rightCell : Cell = getRightCell( cell.xpos, cell.ypos );
					if ( rightCell != null)
					{
						rightCell.removeLeftWall();
						cell.removeRightWall();
					}
				}
				
				if( cell.hasDoorLeft())
				{
					var leftCell : Cell = getLeftCell( cell.xpos, cell.ypos );
					if ( leftCell != null)
					{
						leftCell.removeRightWall();
						cell.removeLeftWall();
					}
				}
				
				if( cell.hasDoorBottom())
				{
					cell.removeBottomWall();
					var bottomCell : Cell = getBottomCell( cell.xpos, cell.ypos );
					if ( bottomCell != null)
					{
						bottomCell.removeTopWall();
					}
				}
			}
		}
		
		public function addItemOnRandomEmptyCell( item : Number ) : void
		{
			var success : Boolean = false ;
			do 
			{
				var t : MathUtils;
				var roll : int = MathUtils.getRandomNumberInRange(0, height * width -1);
				var cell : Cell = cells[roll];
				if ( cell.canHoldItem() )
				{
					cell.item = item;
					success = true;
				}
			}while(!success );
		}
		
		public function addEventWarpEventOnEmptyCells( ) : void
	    {
			var success : Boolean = false ;
			do 
			{
				var t : MathUtils;
				var roll : int = MathUtils.getRandomNumberInRange(0, height * width -1);
				var cell : Cell = cells[roll];
				
				var roll2 : int  = MathUtils.getRandomNumberInRange(0, height * width -1);
				var cell2 : Cell = cells[roll2];
				if ( cell.canHoldItem() && cell2.canHoldItem() && cell != cell2 )
				{
					var warpEvent : IMazeEvent= new WarpEvent(cell.xpos, cell.ypos, cell2.xpos , cell2.ypos)
					cell.event = warpEvent;
					success = true;
				}
			}while(!success );
	    }
	}
}