package labyrinth.level
{
	import labyrinth.level.event.IMazeEvent;

	public class Cell
	{
		public var top : Boolean = false;
		public var bottom : Boolean = false;
		public var left : Boolean = false;
		public var right : Boolean = false;
		public var index : int = -1;
		public var xpos : int = -1;
		public var ypos : int = -1;
		public var depth : int = -1;
		public var item : int = -1;
		public var itemParameters : Object;
		public var exit : Boolean = false;
		public var start : Boolean = false;
		public var visited : Boolean = false
		public var event : IMazeEvent;
		
		public var doorTop : uint = 0;
		public var doorBottom : uint = 0;
		public var doorRight : uint = 0;
		public var doorLeft : uint = 0;
			
		public var topCell : Cell;
		public var bottomCell : Cell;
		public var leftCell : Cell;
		public var rightCell : Cell;
		
		
		
		public function Cell()
		{
		}
		
		public function hasDoorTop() : Boolean
		{
			return doorTop != 0;
		}
		
		public function hasDoorRight() : Boolean
		{
			return doorRight != 0;
		}
		
		public function hasDoorBottom() : Boolean
		{
			return doorBottom != 0;
		}
		
		public function hasDoorLeft() : Boolean
		{
			return doorLeft != 0;
		}
		
		public function removeTopWall() : void
		{
			top = false;
		}
		
		public function removeLeftWall() : void
		{
			left = false;
		}
		
		public function removeRightWall() : void
		{
			right = false;
		}
		
		public function removeBottomWall() : void
		{
			bottom = false;
		}
		
		public function setAllWalls() : void
		{
			top = bottom = left = right = true;
		}
		
		public function setWalls( top : Boolean , right : Boolean , bottom : Boolean , left : Boolean ) : void
		{
			this.top = top;
			this.bottom = bottom;
			this.left = left;
			this.right = right;
		}
		
		public function canHoldItem() : Boolean
		{
			return item == -1 && !exit && event != -1;
		}
		
		public function canHaveEvent() : Boolean
		{
			return item == -1 && !exit && event != null;
		}
		
		public function hasItem() : Boolean
		{
			return item != -1;
		}
		
		public function hasEvent() : Boolean
		{
			return event != null;
		}
	}
}