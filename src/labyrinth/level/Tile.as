package labyrinth.level
{
	
	import flash.display.Bitmap;
	import flash.display.CapsStyle;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	
	import labyrinth.level.Item.Item;
	import labyrinth.level.event.IMazeEvent;
	import labyrinth.utils.DisplayUtils;
	import labyrinth.utils.ImageUtils;
	
	public class Tile extends Sprite{
		
		public static var TILE_SIZE : int = 25;
		
		private var color : uint;
		private var walls : Array;
		
		private var top : Boolean;
		private var bottom : Boolean;
		private var right : Boolean;
		private var left : Boolean;
		
		private var items : Array;
		
		private var isExit : Boolean = false;
		private var item : Item;
		private var itemBitmap : Bitmap;
		private var depth : int = 0;
		private var xpos : int;
		private var ypos  : int
		
		private var event : IMazeEvent;
		private var eventBitmap : Bitmap;
		
		public var hasTorch : Boolean;
		
		public var doorTop : uint = 0;
		public var doorBottom : uint = 0;
		public var doorRight : uint = 0;
		public var doorLeft : uint = 0;
		public var doorSprite : Sprite;
		
		public function Tile( type : Number = 0 )
		{
			switch(type)
			{
				case 0:
					color = 0x000000;
					break;
				case 1:
					color = 0xFF0000;
					break;
				case 2:
					color = 0x00FF00;
					break;
				default:
					color = 0x0000FF;
					break;
				
			}
		
			this.graphics.beginFill(color/*The color for shape*/,1);
			//turning the shape into a square
			this.graphics.drawRect(0,0,TILE_SIZE + 1,TILE_SIZE +1);
			
			this.graphics.lineStyle(3,0xffffff, 1 , true, "normal" , CapsStyle.NONE);
			
			hasTorch = false;
			doorSprite = new Sprite();
		}
		
		
		
		public function setWalls( top : Boolean, right : Boolean , bottom : Boolean , left : Boolean ) : void
		{
			this.left = left;
			this.right = right;
			this.top = top;
			this.bottom = bottom;
			
		}
		
		public function draw() : void
		{
			graphics.clear();
			this.graphics.lineStyle(2,0xffffff);
			
			if( right && doorRight == 0)
			{
				this.graphics.moveTo(TILE_SIZE-1,0);
				this.graphics.lineTo(TILE_SIZE-1,TILE_SIZE-1);
			}
			
			if( left && doorLeft == 0)
			{
				this.graphics.moveTo(-1,0);
				this.graphics.lineTo(-1,TILE_SIZE);
			}
			
			if ( top && doorTop == 0)
			{
				this.graphics.moveTo(0,-1);
				this.graphics.lineTo(TILE_SIZE-1,-1);
			}
			
			if ( bottom && doorBottom == 0)
			{
				this.graphics.moveTo(0,TILE_SIZE-1);
				this.graphics.lineTo(TILE_SIZE-1,TILE_SIZE-1);
			}
			
			drawDoors();
		}
		
		public function drawDoors() : void
		{
			doorSprite.graphics.clear();
			if ( doorLeft != 0 )
			{
				DisplayUtils.setDoorLineStyle(doorSprite, doorLeft);
				doorSprite.graphics.moveTo(0,0);
				doorSprite.graphics.lineTo(0,TILE_SIZE);
			}
			
			if ( doorRight != 0 )
			{
				DisplayUtils.setDoorLineStyle(doorSprite, doorRight);
				doorSprite.graphics.moveTo(TILE_SIZE,0);
				doorSprite.graphics.lineTo(TILE_SIZE,TILE_SIZE);
			}
			
			if ( doorTop != 0 )
			{
				DisplayUtils.setDoorLineStyle(doorSprite, doorTop);
				doorSprite.graphics.moveTo(0,0);
				doorSprite.graphics.lineTo(TILE_SIZE,0);
			}
			
			if ( doorBottom != 0 )
			{
				DisplayUtils.setDoorLineStyle(doorSprite, doorBottom);
				doorSprite.graphics.moveTo(TILE_SIZE,TILE_SIZE);
				doorSprite.graphics.lineTo(0,TILE_SIZE);
			}
			
			addChild(doorSprite);
		}
		
		public function canGoRight() : Boolean
		{
			return !right && doorRight == 0;
		}
		
		public function testRightSideDoor( keys : Dictionary ) : Boolean
		{
			return testSideForDoorsAndKeys(doorRight, keys);
		}
		
		public function removeRightDoor() : void 
		{
			doorRight = 0;
			this.right = false;
			draw();
		}
		
		public function removeLeftDoor() : void 
		{
			doorLeft = 0;
			this.left = false;
			draw();
		}
		
		public function removeTopDoor() : void 
		{
			doorTop = 0;
			this.top = false;
			draw();
		}
		
		public function removeBottomDoor() : void 
		{
			doorBottom = 0;
			this.bottom = false;
			draw();
		}
		
		public function removeDoors(color : uint) : void
		{
			if( doorTop == color ) 
				removeTopDoor();
			
			if( doorBottom == color)
				removeBottomDoor();
			
			if ( doorLeft == color )
				removeLeftDoor();
			
			if ( doorRight == color )
				removeRightDoor();
		}
		
		
		
		public function testTopSideDoor( keys : Dictionary ) : Boolean
		{
			return testSideForDoorsAndKeys(doorTop, keys);
		}
		
		public function testLeftSideDoor( keys : Dictionary ) : Boolean
		{
			return testSideForDoorsAndKeys(doorLeft, keys);
		}
		
		public function testBottomSideDoor( keys : Dictionary ) : Boolean
		{
			return testSideForDoorsAndKeys(doorBottom, keys);
		}
		
		private function testSideForDoorsAndKeys( sideColor : uint , keys : Dictionary ) :Boolean
		{
			if ( sideColor == 0 )
				return false;
			
			if(keys[sideColor] !== undefined)
				return true;
			return false;
		}
		
		public function canGoLeft() : Boolean
		{
			return !left && doorLeft == 0;
		}
		
		public function canGoTop() : Boolean
		{
			return !top && doorTop == 0;
		}
		
		public function canGoDown() : Boolean
		{
			return !bottom && doorBottom == 0;
		}
		
		public function setExit(): void
		{
			isExit = true;
			// draw exit 
			this.graphics.beginFill(0xffffff);
			this.graphics.drawRect(TILE_SIZE / 4, TILE_SIZE / 4, TILE_SIZE / 2, TILE_SIZE / 2);
		}
		
		public function testExit() : Boolean
		{
			return isExit;
		}
		
		public function addItem( item : Item ) : void
		{
			this.item = item;
			this.itemBitmap = item.getBitmap();
			this.addChild(itemBitmap);
		}
		
		
		
		public function hasItem() : Boolean
		{
			return item != null;
		}
		
		public function getItem() : Item
		{
			return item;
		}
		
		public function getItemAction( level : Level ) : void
		{
			item.getItemAction(level);
			this.removeChild(itemBitmap);
			item = null;
			itemBitmap = null;
		}
		
		public function addEvent( event : IMazeEvent ) : void
		{
			this.event = event;
			this.eventBitmap = event.getBitmap();
			this.addChild(eventBitmap);
		}
		
		public function hasEvent() : Boolean
		{
			return event != null;
		}
		
		public function triggerEvent( level : Level ) : void
		{
			event.triggerEvent(level);
		}
		
		public function putTorch() : void
		{
			hasTorch = true;
			var bitmap : Bitmap = ImageUtils.getLitTorchIcon();
			// todo : position of torch seems odd. will probably change

			this.addChild(bitmap);
		}
	
	}
}