package labyrinth.level.effect
{
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import labyrinth.level.Tile;
	import labyrinth.utils.MathUtils;

	public class WarpEffect
	{
		
		private var timer : Timer;
		private var s : Sprite;
		private var craps : Array
		private var count : int = 0;
		
		public function WarpEffect( s : Sprite )
		{
			craps = new Array();
			timer = new Timer(10,40);
			this.s = s;
			timer.addEventListener(TimerEvent.TIMER, onWarp);
			timer.addEventListener(TimerEvent.TIMER_COMPLETE , onWarpComplete);
		}
		
		public function start() : void 
		{
			timer.start();
		}
		
		private function onWarp( e : TimerEvent ) : void 
		{
			if( count == 3)
			{
				var ray : Sprite = new Sprite;
				ray.graphics.lineStyle(2,0xffffff);
				ray.graphics.lineTo(0,5);
				ray.x = MathUtils.getRandomNumberInRange(0,Tile.TILE_SIZE);
				ray.y = MathUtils.getRandomNumberInRange(0,3);
				craps.push(ray);
				this.s.addChild(ray);
				count = 0
			}
			
			
			for each ( var crap : Sprite in craps ) 
			{
				crap.y -= 1;
			}
			
			count ++;
		}
		
		private function onWarpComplete( e : TimerEvent ) : void 
		{
			for each ( var crap : Sprite in craps ) 
			{
				s.removeChild(crap);
			}
			timer.stop();
			timer.removeEventListener(TimerEvent.TIMER, onWarp);
			timer.removeEventListener(TimerEvent.TIMER_COMPLETE , onWarpComplete);
		}
	}
}