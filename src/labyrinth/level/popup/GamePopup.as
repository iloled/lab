package labyrinth.level.popup
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display.Stage;
	
	import labyrinth.utils.DisplayUtils;

	public class GamePopup
	{
		public function GamePopup()
		{
		}
		
		public static function createPopup( stage : Stage,
											width : int ,
											height : int,
											color : uint = 0x000000
		) : Sprite 
		{
			var d : DisplayUtils;
			var sprite : Sprite = new Sprite();
			DisplayUtils.fillRectSprite(sprite, color , width,height);
			DisplayUtils.drawBorder(sprite,0xffffff,5);
			
			//===
			
			centerPopup( stage , sprite );
			
			stage.addChild(sprite);
			
			return sprite;
			
		}
		
		private static function centerPopup( container : DisplayObject , element : DisplayObject ) : void
		{
			element.x = (container.width - element.width) / 2 ;
			element.y = (container.height - element.height) / 2 ;
		}
	}
}