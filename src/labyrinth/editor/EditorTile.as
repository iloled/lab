package labyrinth.editor
{
	import flash.display.CapsStyle;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import labyrinth.level.Item.ItemFactory;
	import labyrinth.level.event.EventFactory;
	import labyrinth.level.event.IMazeEvent;
	import labyrinth.level.event.SwitchEvent;
	import labyrinth.level.event.WarpEvent;
	import labyrinth.utils.DisplayUtils;
	import labyrinth.utils.ImageUtils;
	
	public class EditorTile extends Sprite
	{
		
		public static var TILE_SIZE : int = 40;
		public var top : Boolean = false;
		public var bottom : Boolean = false;
		public var left : Boolean = false;
		public var right : Boolean = false;
		public var index : int = -1;
		public var xpos : int = -1;
		public var ypos : int = -1;
		public var depth : int = -1;
		
		public var rightSprite : Sprite ;
		public var leftSprite : Sprite ;
		public var topSprite : Sprite ;
		public var bottomSprite : Sprite ;
		
		public var item : int = -1;
		public var itemSprite : DisplayObject;
		private var itemParameters : Object ;
		public var exit : Boolean;
		private var exitIcon : DisplayObject;
		public var start : Boolean;
		private var startIcon : DisplayObject;
		
		public var event : int = -1;
		public var eventSprite : Sprite;
		public var eventParameters : Object;
	
		public var doorTop : uint = 0;
		public var doorBottom : uint = 0;
		public var doorRight : uint = 0;
		public var doorLeft : uint = 0;
		
		
		private var center : Sprite;
		
		private var editor : LevelEditor;
		private var warpSprite : DisplayObject;
		
		public function EditorTile( editor : LevelEditor )
		{
			super();
			
			this.editor = editor;
			center = new Sprite();
			center.graphics.clear();
			center.graphics.beginFill( 0x0000FF, 1 );
			center.graphics.drawRect(6, 6, TILE_SIZE-10, TILE_SIZE-10);
			center.graphics.endFill();
			
			addChild(center);
			
			center.addEventListener(MouseEvent.CLICK, onClickCenter);
			
			rightSprite = new Sprite;
			addChild(rightSprite);
			rightSprite.x = TILE_SIZE -2;
			rightSprite.addEventListener(MouseEvent.CLICK, onClickRight);
			
			leftSprite = new Sprite;
			addChild(leftSprite);
			leftSprite.x = 1;
			leftSprite.addEventListener(MouseEvent.CLICK, onClickLeft);
			
			topSprite = new Sprite;
			addChild(topSprite);
			topSprite.y = 1;
			topSprite.addEventListener(MouseEvent.CLICK, onClickTop);
			
			bottomSprite = new Sprite;
			addChild(bottomSprite);
			bottomSprite.y = TILE_SIZE -2;
			bottomSprite.addEventListener(MouseEvent.CLICK, onClickBot);
			
			draw();
			
			exit = false;
			start = false;
			
		}
		
		public function setStart() : void
		{
			start = true;
		}
		
		public function toggleExit() : void
		{
			if( exit ) 
			{
				exit = false;
				this.removeChild(this.exitIcon);
			}
			else
			{
				exit = true;
				this.exitIcon = 
					ImageUtils.changeBitmapSize(ImageUtils.getExitIcon(), TILE_SIZE - 4 , TILE_SIZE -4 );
				exitIcon.x = 2;
				exitIcon.y = 2;
				this.addChild(exitIcon);
			}
		}
		
		public function toggleStart() : void
		{
			if( start ) 
			{
				start = false;
				this.removeChild(this.startIcon);
			}
			else
			{
				start = true;
				this.startIcon = 
					ImageUtils.changeBitmapSize(ImageUtils.getStartIcon(), TILE_SIZE - 4 , TILE_SIZE -4);
				startIcon.x = 2;
				startIcon.y = 2;
				this.addChild(startIcon);
			}
		}
		
		public function hasItemOrEvent() : Boolean
		{
			return this.item != -1 || event != -1;
		}
		
		public function invertWalls() : void
		{
			left = !left;
			right = !right;
			bottom = !bottom;
			top = !top;
			draw();
		}
		
		public function setItem( item : int , itemParameters : Object = null ) : void
		{
			if ( itemSprite == null ) 
			{
				this.item = item;
				this.itemSprite = ImageUtils.changeBitmapSize(ItemFactory.createItemByNumber(item,itemParameters).getBitmap(), TILE_SIZE- 4 , TILE_SIZE- 4 );
				this.addChild(itemSprite);
				itemSprite.x = 2;
				itemSprite.y = 2;
			}
			else
			{
				this.item = -1;
				this.removeChild(itemSprite);
				this.itemSprite = null;
			}
			
			this.itemParameters = itemParameters;
		}
		
		
		public function setWarpEventSource( warp : WarpEvent, warpNumber : int) : void
		{
			this.event = EventFactory.WARP;
			this.eventSprite = new Sprite;
			eventSprite.addChild(
				ImageUtils.changeBitmapSize(warp.getBitmap(), TILE_SIZE- 4 , TILE_SIZE- 4 ));
			var text : TextField = DisplayUtils.createTextWithDefaultFormat( warpNumber + "");
			text.x = 12;
			text.y = 10;
			text.mouseEnabled = false;
			text.selectable = false;
			eventSprite.addChild(text);
			eventSprite.mouseEnabled = false;
			this.addChild(eventSprite);
			eventSprite.x = 2;
			eventSprite.y = 2;
		}
		
		public function addEvent( eventType : int , parameters : Object = null) : void 
		{
			this.event = eventType;
			var event : IMazeEvent = EventFactory.createEventByNumber(eventType, parameters);
			this.eventSprite = new Sprite;
			eventSprite.addChild(
				event.getBitmap());
			eventSprite.x = 7;
			eventSprite.y = 7;
			addChild(eventSprite);
			this.eventParameters = parameters;
		}
		
		
		
		public function setWarpEventDestination( warpNumber : int ) : void
		{
			var text : TextField = DisplayUtils.createTextWithDefaultFormat( warpNumber + "");
			text.mouseEnabled = false;
			text.selectable = false;
			text.x = 12;
			text.y = 10;
			warpSprite = text;
			this.addChild(text);
		}
		
		public function removeDestination() : void 
		{
			removeChild(warpSprite);
		}
		
		public function removeEvent() : void 
		{
			removeChild(eventSprite);
			event = -1;
		}
		
		
		
		private function onClickCenter( e : MouseEvent ) : void
		{
			editor.handleTileClick(this);
		}
		
		private function onClickRight( e : MouseEvent ) : void
		{
			right = !right;
			if( editor.nextWallIsDoor() ) 
			{
				this.doorRight = editor.getDoorColorAndReset();
			}
			else
			{
				this.doorRight = 0;
			}
			draw();
		}
		
		private function onClickLeft( e : MouseEvent ) : void
		{
			left = !left;
			if( editor.nextWallIsDoor() ) 
			{
				this.doorLeft = editor.getDoorColorAndReset();
			}
			else
			{
				this.doorLeft = 0;
			}
			draw();
		}
		
		private function onClickTop( e : MouseEvent ) : void
		{
			top = !top;
			if( editor.nextWallIsDoor() ) 
			{
				this.doorTop = editor.getDoorColorAndReset();
			}
			else
			{
				this.doorTop = 0;
			}
			draw();
		}
		
		private function onClickBot( e : MouseEvent ) : void
		{
			bottom = !bottom;
			if( editor.nextWallIsDoor() ) 
			{
				this.doorBottom = editor.getDoorColorAndReset();
			}
			else
			{
				this.doorBottom = 0;
			}
			draw();
		}
		
		public function getJSONData() : String 
		{
			return JSON.stringify(getData());
		}
		
		public function getData() : Object
		{
			var result : Object = 
				{ 
					top : top,
					bottom : bottom,
					left : left,
					right : right,
					item : item,
					itemParameters : itemParameters,
					exit : exit,
					index : index,
					x : xpos,
					y : ypos,
					depth : depth,
					doorTop : doorTop,
					doorLeft : doorLeft,
					doorRight : doorRight,
					doorBottom : doorBottom,
					event : this.event,
					eventParameter : this.eventParameters
				};
			
			return result;
		}

		public function draw() : void
		{
			this.graphics.clear();
			this.graphics.beginFill( 0x000000, 1 );
			this.graphics.drawRect(0, 0, TILE_SIZE, TILE_SIZE);
			this.graphics.endFill();
		
			rightSprite.graphics.clear();
			if( right )
			{
				rightSprite.graphics.lineStyle(4, 0xFFFFFF,1,true,"normal",CapsStyle.NONE);
			}
			else
			{
				rightSprite.graphics.lineStyle(4, 0x555555,1,true,"normal",CapsStyle.NONE);
			}
			
			rightSprite.graphics.moveTo(0,0);
			rightSprite.graphics.lineTo(0,TILE_SIZE);
			
			if( left )
			{
				leftSprite.graphics.lineStyle(4, 0xFFFFFF,1,true,"normal",CapsStyle.NONE);
			}
			else
			{
				leftSprite.graphics.lineStyle(4, 0x555555,1,true,"normal",CapsStyle.NONE);
			}
			
		
			
			leftSprite.graphics.moveTo(0,0);
			leftSprite.graphics.lineTo(0,TILE_SIZE);
			
			
			if ( top )
			{
				topSprite.graphics.lineStyle(4, 0xFFFFFF,1,true,"normal",CapsStyle.NONE);
			}
			else
			{
				topSprite.graphics.lineStyle(4, 0x555555,1,true,"normal",CapsStyle.NONE);
			}
			
			topSprite.graphics.moveTo(0,0);
			topSprite.graphics.lineTo(TILE_SIZE,0);
			
			if ( bottom )
			{
				bottomSprite.graphics.lineStyle(4, 0xFFFFFF,1,true,"normal",CapsStyle.NONE);
			}
			else
			{
				bottomSprite.graphics.lineStyle(4, 0x555555,1,true,"normal",CapsStyle.NONE);
			}
			
			bottomSprite.graphics.moveTo(0,0);
			bottomSprite.graphics.lineTo(TILE_SIZE,0);
			
			// display doors
			
			if( doorTop != 0 )
			{
				DisplayUtils.setDoorLineStyle(this, doorTop);
				this.graphics.moveTo(0,0);
				this.graphics.lineTo(TILE_SIZE,0);
			}
			
			if( doorRight != 0 )
			{
				rightSprite.graphics.clear();
				DisplayUtils.setDoorLineStyle(rightSprite, doorRight);
				rightSprite.graphics.moveTo(0,0);
				rightSprite.graphics.lineTo(0,TILE_SIZE);
			}
			
			if( doorBottom != 0 )
			{
				bottomSprite.graphics.clear();
				DisplayUtils.setDoorLineStyle(bottomSprite, doorBottom);
				bottomSprite.graphics.moveTo(0,0);
				bottomSprite.graphics.lineTo(TILE_SIZE,0);
				
			}
			
			if( doorLeft != 0 )
			{
				DisplayUtils.setDoorLineStyle(this, doorLeft);
				this.graphics.moveTo(0,0);
				this.graphics.lineTo(0,TILE_SIZE);
			}
		}
	}
}