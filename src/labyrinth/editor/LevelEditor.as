package labyrinth.editor
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	
	import labyrinth.level.Cell;
	import labyrinth.level.Item.ItemFactory;
	import labyrinth.level.Maze;
	import labyrinth.level.data.LevelGenerator;
	import labyrinth.level.event.EventFactory;
	import labyrinth.level.event.SwitchEvent;
	import labyrinth.level.event.WarpEvent;
	import labyrinth.utils.DisplayUtils;
	import labyrinth.utils.ImageUtils;
	import labyrinth.utils.PopupManager;
	
	public class LevelEditor extends Sprite
	{
		
		private var tiles : Array;
		private var gridWidth : int ;
		private var gridHeight : int;
		private var baseTime : int;
		
		// side menu
		
		private var widthInput : TextField;
		private var heightInput : TextField;
		private var timeInput : TextField;
		
		private var rightMenu : Sprite;
		private var toolBar : Sprite;
		
		private var export : TextField;
		private var bottomScrollBar : Sprite;
		private var bottomScroll : Sprite;
		private var rightScrollBar : Sprite;
		private var rightScroll : Sprite;
		
		private var invertButtonMode : Sprite;
		private var pathButtonMode : Sprite;
		private var itemMode : Sprite;
		private var exitButton : Sprite;
		private var itemList : Sprite;
		private var startButton : Sprite;
		
		// size config
		
		private var editorSize : int = 500;
		private var sideBarSize : int = 150;
		private var scrollBarSize : int = 15;
		private var toolBarSize : int = 30;
		
		// mask
		private var m : Sprite;
		
		private var tileContainer : Sprite;
		
		private var contentWidth : int ;
		private var contentHeight : int;
		
		private static var INVERT : int = 1;
		private static var PATH : int = 2;
		private static var EXIT : int = 4;
		private static var PLACE_ITEM : int = 3;
		private static var START : int = 5;
		private static var WARP_DESTINATION : int = 6;
		private static var WARP_SOURCE : int = 7;
		private static var PLACE_STEP : int = 8;
		
		private var startTile : EditorTile;
		
		private static var currentItem : int = 0;
		
		private var editorPopup : EditorPopup;
		private var generator : LevelGenerator;
		
		private var mode : int;
		
		// destination tile
		
		private var warpSourceTile : EditorTile;
		private var warps : Array = new Array();
		private var warpIndex : int = 0;
		
		// door
		
		private var doorColor : uint = 0;
		private var currentKeyColor : uint = 0;
		
		// random crap
		
		private var stepColor : uint = 0;
		private var steps : Array = new Array();
		
		
		public function LevelEditor()
		{
			super();
			generator = new LevelGenerator();
			initConfigs();
			
			tileContainer = new Sprite;
			addChild(tileContainer);
			
			initGrid();
			
			editorPopup = new EditorPopup(this);
			
			this.x = 0;
			this.graphics.clear();
			this.graphics.beginFill( 0xFF0000, 1 );
			this.graphics.drawRect(0, 0, editorSize, editorSize);
			this.graphics.endFill();
			
			rightMenu = new Sprite();
			rightMenu.x = editorSize;
			DisplayUtils.fillRectSprite(rightMenu, 0x222222, 150, editorSize);
			
			addChild(rightMenu);
			
			var widthLabel : TextField  = DisplayUtils.createTextWithDefaultFormat("Width");
			widthLabel.width = 75;
			widthLabel.y = 5;
			
			rightMenu.addChild(widthLabel);
			
			widthInput = DisplayUtils.createTextInput(50,20,"" + gridWidth);
			widthInput.x = 70;
			widthInput.y = 10;
			
			rightMenu.addChild(widthInput);
			var heightLabel : TextField  = DisplayUtils.createTextWithDefaultFormat("Height");
			heightLabel.y = 40;
			
			rightMenu.addChild(heightLabel);
			
			heightInput = DisplayUtils.createTextInput(50,20,"" + gridHeight);
			heightInput.y = 40;
			heightInput.x = 70;
			
			rightMenu.addChild(heightInput);
			
			var timeLabel : TextField = DisplayUtils.createTextWithDefaultFormat("Base Time");
			timeLabel.y = 70;
			rightMenu.addChild(timeLabel);
			
			timeInput = DisplayUtils.createTextInput(50,20, "" + baseTime);
			timeInput.y = 70;
			timeInput.x = 90;
			
			rightMenu.addChild(timeInput);
			
			invertButtonMode = DisplayUtils.createButton("Invert");
			invertButtonMode.y = 100;
			invertButtonMode.addEventListener(MouseEvent.CLICK, setInvertMode);
			
			rightMenu.addChild(invertButtonMode);
			
			pathButtonMode = DisplayUtils.createButton("path");
			pathButtonMode.y = 100;
			pathButtonMode.x = 75;
			pathButtonMode.addEventListener(MouseEvent.CLICK, setPathMode);
			rightMenu.addChild(pathButtonMode);
			
			itemMode = DisplayUtils.createButton("Item");
			itemMode.y = 135;
			itemMode.addEventListener(MouseEvent.CLICK, setItemMode);
			
			rightMenu.addChild(itemMode);
			
			exitButton = DisplayUtils.createButton("Exit");
			exitButton.y = 170;
			exitButton.addEventListener(MouseEvent.CLICK, setExitMode);
			
			rightMenu.addChild(exitButton);
			
			startButton = DisplayUtils.createButton("Start");
			startButton.y = 170;
			startButton.x = 75;
			startButton.addEventListener(MouseEvent.CLICK, setStartMode);
			
			rightMenu.addChild(startButton);
			
			itemList = new Sprite;
			itemList.y = 135;
			itemList.x = 70;
			
			rightMenu.addChild(itemList);
			
			initItemList();
			
			var reloadButton : Sprite = DisplayUtils.createButton("Reload");
			reloadButton.addEventListener(
				MouseEvent.CLICK, 
				function( e : MouseEvent ) : void {  reloadGrid() } 
			) ;
			
			rightMenu.addChild(reloadButton);
			
			reloadButton.y = 200;

			
			var generate : Sprite = DisplayUtils.createButton("Generate");
			generate.y = 200;
			generate.x = 100;
			rightMenu.addChild(generate);
			generate.addEventListener(MouseEvent.CLICK, function() : void { generateJSON(); });
			
			export = DisplayUtils.createTextInput(this.sideBarSize,200,"" );
			export.y = 250;
			
			rightMenu.addChild(export);
			
			m = new Sprite;
			this.m .graphics.clear();
			this.m .graphics.beginFill( 0xFF0000, 1 );
			this.m .graphics.drawRect(0, 0, editorSize + sideBarSize + toolBarSize, editorSize);
			this.m .graphics.endFill();
			this.mask  = m;
			
			
			initScrollBar();
			initToolBar();
			
			
		}
		
	
		
		public function cellsToEditorTile( maze : Maze ) : void
		{
			var cells : Vector.<Cell> = maze.getCells();
			resetEditorData()
			tiles = new Array();
			var x : int = 0;
			var y : int = 0;
			
			for( var i : int = 0 ; i < gridHeight ; i++ )
			{
				x = 0;
				for( var j : int = 0 ; j < gridWidth ; j ++ ) 
				{
					var index : int = i*gridWidth + j;
					var cell : Cell = cells[index];
					var tile : EditorTile = new EditorTile(this);
					tile.x = x;
					tile.y = y;
					tile.xpos = j;
					tile.ypos = i;
					
					
					x+= EditorTile.TILE_SIZE;
					tile.index = tiles.length;
					tiles.push(tile);
					
					tile.top = cell.top;
					tile.left = cell.left;
					tile.right = cell.right;
					tile.bottom = cell.bottom;
					
					if( cell.start ) 
						tile.toggleStart();
					
					tile.draw();
					
					tileContainer.addChild(tile);
				}
				y+= EditorTile.TILE_SIZE;
			}
		
		}
		
		private function generateEmptyGrid() : void
		{
			tiles = new Array();
			var x : int = 0;
			var y : int = 0;
			
			for( var i : int = 0 ; i < gridHeight ; i++ )
			{
				x = 0;
				for( var j : int = 0 ; j < gridWidth ; j ++ ) 
				{
					var tile : EditorTile = new EditorTile(this);
					tile.x = x;
					tile.y = y;
					tile.xpos = j;
					tile.ypos = i;
					
					
					x+= EditorTile.TILE_SIZE;
					tile.index = tiles.length;
					tiles.push(tile);
					
					tileContainer.addChild(tile);
				}
				y+= EditorTile.TILE_SIZE;
			}
		}
		
		private function resetEditorData() : void 
		{
			tileContainer.removeChildren();
			resetWarps();
		}
		
		public function loadJSON( json : String ) : void
		{
			var data : Object = JSON.parse(json);
			var datas : Array = data.tiles;
			resetEditorData();
			gridWidth = data.width
			gridHeight = data.height
			var x : int = 0;
			var y : int = 0;
			if( data.start != null )
			{
				var startX : int = data.start.x;
				var startY : int = data.start.y;
			}
			
			tiles = new Array();
			for( var i : int = 0 ; i < gridHeight ; i++ )
			{
				x = 0;
				for( var j : int = 0 ; j < gridWidth ; j ++ ) 
				{
					var index : int = j + i * gridWidth;
					var tile : EditorTile = new EditorTile(this);
					tile.x = x;
					tile.y = y;
					tile.xpos = j;
					tile.ypos = i;
					
					x+= EditorTile.TILE_SIZE;
					tile.index = tiles.length;
					tiles.push(tile);
					tile.top = datas[index].top;
					tile.bottom = datas[index].bottom;
					tile.right = datas[index].right;
					tile.left = datas[index].left;
					tile.doorBottom = datas[index].doorBottom;
					tile.doorLeft = datas[index].doorLeft;
					tile.doorRight = datas[index].doorRight;
					tile.doorTop = datas[index].doorTop;
					
					
					tileContainer.addChild(tile);
					
					if( datas[index].exit ) 
					{
						tile.toggleExit();
					}
					
					if( i == startY && j == startX ) 
					{
						tile.toggleStart();
						startTile = tile; 
					}
					
					if(datas[index].item != -1)
					{
						var d = datas[index];
						tile.setItem(datas[index].item, datas[index].itemParameters );
					}
					tile.draw();
				}
				y+= EditorTile.TILE_SIZE;
			}
			
			
			
			for each ( var warp : Object in data.warps ) 
			{
				var w : WarpEvent = new WarpEvent(
					warp.sourceX ,warp.sourceY,
					warp.destX, warp.destY
				);
				
				warps.push(w);
			
				var sourceTile : EditorTile = tiles[ warp.sourceX + warp.sourceY * gridWidth];
				sourceTile.setWarpEventSource(w, warpIndex);
				
				var destinationTile : EditorTile = tiles[ warp.destX + warp.destY * gridWidth];
				destinationTile.setWarpEventDestination(warpIndex);
				warpIndex ++;
				sourceTile.draw();
			}
			
			resetScrollBar();
			
			
				
			
		}
		
		private function resetScrollBar() : void 
		{
			if ( bottomScrollBar != null)
			{
				this.removeChild(bottomScrollBar);
				bottomScrollBar = null;
			}
			
			if ( rightScrollBar  != null ) 
			{
				this.removeChild(rightScrollBar);
				rightScrollBar = null;
			}
			
			contentWidth = tileContainer.width;
			contentHeight = tileContainer.height;
			initScrollBar();
		}
		
		private function resetWarps() : void 
		{
			warps = new Array();
			warpIndex = 0;
		}
		
		private function initToolBar() : void
		{
			toolBar = new Sprite();
			toolBar.x = editorSize + sideBarSize;
			
			DisplayUtils.fillRectSprite( toolBar , 0x222222 , toolBarSize , editorSize);
			
			addChild(toolBar);
			
			addIconToToolBar( 
				ImageUtils.getIconWithBackground(ImageUtils.getWriteIcon()),
				function() : void { editorPopup.displayLoadJSONPopup(); }
			);
			
			addIconToToolBar( 
				ImageUtils.getIconWithBackground(ImageUtils.getWarpIcon()),
				warpClick
			);
			
			addIconToToolBar( 
				ImageUtils.getIconWithBackground(ImageUtils.getLockIcon()),
				function() : void 
				{
					editorPopup.displayDoorPopup();
				}
			);
			
			addIconToToolBar( 
				ImageUtils.getIconWithBackground(ImageUtils.getStepIcon(),0xFF0000),
				function() : void 
				{
					editorPopup.displayStepPopup();
				}
			);
			
			addIconToToolBar( 
				ImageUtils.getIconWithBackground(ImageUtils.getSettingsIcon(),0xFF0000),
				function() : void 
				{
					editorPopup.displayGenerationPopup();
				}
			);
		}
		
		private function addIconToToolBar( icon : Sprite , callback : Function ) : void 
		{
			var toolBarSize : int = toolBar.numChildren * 30;	
			icon.x = 5;
			icon.y = toolBarSize;
			icon.addEventListener(MouseEvent.CLICK, callback);
			toolBar.addChild(icon);	
		}
		
		private function warpClick( e : MouseEvent ) : void
		{
			editorPopup.displayWarpPopup( warps );
		}
		
		private function switchClick( e : MouseEvent ) : void 
		{
			editorPopup.displayStepPopup();
		}
		
		public function addNewWarp() : void
		{
			editorPopup.displayTimedPopup("Select warp \nsource then destination"); 
			this.setSourceMode();
		}
		
		public function deleteWarp( index : int ) : void
		{
			var warp : WarpEvent = warps[index];
			var sourceTile : EditorTile = tiles[warp.sourceX + warp.sourceY * this.gridWidth];
			sourceTile.removeEvent();
			var destinationTile : EditorTile = tiles[warp.destX + warp.destY * this.gridWidth];
			destinationTile.removeDestination();
			warps.splice(index,1);
		}
		
		// TODO : Door - low quality code, rushed a bit but works
		
		public function setDoorColor( color : uint ) : void 
		{
			doorColor = color;
		}
		
		public function setStepColor( color : uint ) : void 
		{
			stepColor = color
			mode = PLACE_STEP;
		}
		
		public function setKeyColor( color : uint ) : void 
		{
			this.currentKeyColor = color;
		}
		
		public function nextWallIsDoor() : Boolean 
		{
			return doorColor != 0;		
		}
		
		public function getDoorColorAndReset() : uint
		{
			var tmp : uint = doorColor;
			doorColor = 0;
			return tmp;
		}

		private function setInvertMode( e : MouseEvent ) : void
		{
			this.mode = INVERT;
			resetButtons();
			setButtonFocus(invertButtonMode);
		}
		
		private function setItemMode ( e : MouseEvent ) : void
		{
			this.mode = PLACE_ITEM;
			resetButtons();
			setButtonFocus(itemMode);
		}
		
		public function setDestinationMode() : void
		{
			this.mode = WARP_DESTINATION;
			resetButtons();
		}
		
		private function setSourceMode() : void 
		{
			this.mode = WARP_SOURCE;
			resetButtons();
		}
		
		private function setExitMode( e : MouseEvent ) : void
		{
			this.mode = EXIT;
			resetButtons();
			setButtonFocus(exitButton);
		}
		
		private function setPathMode( e : MouseEvent ) : void
		{
			this.mode = PATH;
			resetButtons();
			setButtonFocus(pathButtonMode);
		}
		
		private function setStartMode( e : MouseEvent ) : void
		{
			this.mode = START;
			resetButtons();
			setButtonFocus(startButton);
		}
		
		private function resetButtons() : void
		{
			DisplayUtils.fillRectSprite(invertButtonMode, 0xA9A9A9,50, 20);
			DisplayUtils.fillRectSprite(pathButtonMode, 0xA9A9A9,50, 20);
			DisplayUtils.fillRectSprite(itemMode, 0xA9A9A9,50, 20);
			DisplayUtils.fillRectSprite(exitButton, 0xA9A9A9,50, 20);
			DisplayUtils.fillRectSprite(startButton, 0xA9A9A9,50, 20);
				
		}
		
		private function setButtonFocus ( button : Sprite)  : void
		{
			DisplayUtils.fillRectSprite(button, 0xb20000,50, 20);
		}
		
		private function initItemList() : void
		{
			var position : int = 0;
			// TODO : a bit rigid
			for( var i : int = 1 ; i < 5 ; i++ )
			{
				
				var item : TextField = new TextField;
				item.text = i + "";
				item.setTextFormat(DisplayUtils.getDefaultTextFormat());
			
				itemList.addChild(item);
				item.height = 30;
				item.width = 15;
				item.x = position;
				position += 15;
				item.addEventListener(MouseEvent.CLICK, selectItem );
				
				// preselect first item
				if( i == 1 ) 
				{
					item.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
				}
			}
			
		}
		
		private function disableItems() : void
		{
			for( var i : int = 0 ; i < itemList.numChildren ; i ++)
			{
				var item : TextField = itemList.getChildAt(i) as TextField;
				item.background = false;
			}
		}
		
		private function generateJSON() : void
		{
			var data : Object = new Object();
			if( startTile == null)
				data.start = null;
			else
				data.start = { x : startTile.xpos , y : startTile.ypos };
			data.tiles = new Array();
			data.width = this.gridWidth;
			data.height = this.gridHeight;
			data.baseTime = timeInput.text;
			for each ( var tile : EditorTile in tiles ) 
			{
				data.tiles.push(tile.getData());
			}
			
			// json event
			
			data.warps = new Array();
			
			for each ( var warp : WarpEvent in warps ) 
			{
				data.warps.push( 
					{ 
						sourceX : warp.sourceX , 
						sourceY : warp.sourceY , 
						destX : warp.destX,
						destY : warp.destY
					}
				);
			}
			
			export.text = JSON.stringify(data);
			
			
		}
		
		public function selectItem( e : MouseEvent ) : void
		{
			disableItems();
			var target : TextField = e.target as TextField;
			var item : Number = Number(target.text);
			currentItem = item;
			target.background = true;
			target.backgroundColor = 0xFF0000;
			
			var t : ItemFactory;
			if ( item == ItemFactory.KEY ) 
			{
				editorPopup.displayKeyColorPopup();
			}
		}
		
		public function handleTileClick( tile : EditorTile ) : void
		{
			var index : int = tile.index;
			var tile : EditorTile = tiles[index];
			switch( mode ) 
			{
				case WARP_SOURCE:
					if(!tile.hasItemOrEvent())
					{
						warpSourceTile = tile;
						this.setDestinationMode();
						this.editorPopup.displayTimedPopup("Choose destination cell");
					}
					else
					{
						this.editorPopup.displayTimedPopup("Choose empty cell \n for source");
					}
					break;
				case WARP_DESTINATION:
					if(!tile.hasItemOrEvent())
					{
						var warp : WarpEvent = new WarpEvent(
							this.warpSourceTile.xpos ,this.warpSourceTile.ypos,
							tile.xpos, tile.ypos
							);
						warps.push(warp);
						this.warpSourceTile.setWarpEventSource(warp, warpIndex);
						tile.setWarpEventDestination(warpIndex);
						setDefaultMode();
						warpIndex++;
					}
					else
					{
						this.editorPopup.displayTimedPopup("Choose empty cell \n for destination");
					}
					break;
				case PLACE_STEP:
					if(!tile.hasItemOrEvent())
					{
						var t : EventFactory;
						tile.addEvent(EventFactory.SWITCH, { color :  stepColor });
						setDefaultMode();
					}
					else
					{
						this.editorPopup.displayTimedPopup("Choose empty cell \n to place step");
					}
					break;
				case INVERT:
					tile.invertWalls();
					break;
				case PLACE_ITEM:
					var parameters : Object;
					if ( currentItem == ItemFactory.KEY ) 
					{
						parameters = { color : this.currentKeyColor } ;
					}
					tile.setItem( currentItem , parameters ) ;
					break;
				case EXIT:
					tile.toggleExit();
					break;
				case START:
					if(startTile != null)
						startTile.toggleStart();
					startTile = tile;
					tile.toggleStart();
					break;
				case PATH: 
					if( index < gridWidth ) 
					{
						tile.top = true;
					}
					else 
					{
						var topTile : EditorTile = tiles[index - gridWidth ];
						
						if ( topTile.bottom ) 
						{
							topTile.bottom = false;
							topTile.draw();
							tile.top = false;
						}
						else
						{
							tile.top = true;
						}
					}
					
					// right
					
					if ( ( index + 1 ) % gridWidth == 0 )
					{
						tile.right = true;
					}
					else
					{
						var rightTile : EditorTile = tiles[index + 1 ];
						
						if ( rightTile.left ) 
						{
							rightTile.left = false;
							rightTile.draw();
							tile.right = false;
						}
						else
						{
							tile.right = true;
						}
					}
					
					// left
					
					if (  index  % gridWidth == 0 )
					{
						tile.left = true;
					}
					else
					{
						var leftTile : EditorTile = tiles[index - 1];
						
						if ( leftTile.right ) 
						{
							leftTile.right = false;
							leftTile.draw();
							tile.left = false;
						}
						else
						{
							tile.left = true;
						}
					}
					
					// bottom
					
					if ( gridWidth * ( gridHeight - 1 ) < index ) 
					{
						tile.bottom = true;
					}
					else
					{
						var bottomTile : EditorTile = tiles[index + gridWidth];
						
						if ( bottomTile.top ) 
						{
							bottomTile.top = false;
							bottomTile.draw();
							tile.bottom = false;
						}
						else
						{
							tile.bottom = true;
						}
					}
					tile.draw();
					break;
			}
		}
		
		
		private function initGrid() : void
		{
			
			//generateRecursiveBacktracker()
			generateEmptyGrid();
			
			tileContainer.x = 0;
			tileContainer.y = 0;
			
			contentWidth = width;
			contentHeight = height;
		}
		
		public function generateRecursiveBacktracker() : void
		{
			var maze : Maze = generator.recursiveBacktracker(gridWidth,gridHeight);
			this.cellsToEditorTile(maze);
		}
		
		private function reloadGrid() : void
		{
			tileContainer.removeChildren();
			gridWidth = Number(this.widthInput.text)
			gridHeight = Number(this.heightInput.text)
			initGrid();
			if ( bottomScrollBar != null)
			{
				this.removeChild(bottomScrollBar);
				bottomScrollBar = null;
			}
			
			if ( rightScrollBar  != null ) 
			{
				this.removeChild(rightScrollBar);
				
				rightScrollBar = null;
			}
			
			contentWidth = tileContainer.width;
			contentHeight = tileContainer.height;
			initScrollBar();
			addChild(rightMenu);
		}
		
		
		
		private function initScrollBar() : void
		{
			
			if( contentWidth + scrollBarSize > editorSize  ) 
			{
				bottomScrollBar = new Sprite;
				bottomScrollBar.y = editorSize - scrollBarSize;
				bottomScrollBar.graphics.beginFill( 0xD3D3D3, 1 );
				bottomScrollBar.graphics.drawRect(0, 0, editorSize , scrollBarSize);
				bottomScrollBar.graphics.endFill();
				
				addChild(bottomScrollBar);
				
				bottomScroll = new Sprite();
				bottomScroll.graphics.beginFill( 0xA0A0A0, 1 );
				bottomScroll.graphics.drawRect(0, 0, 50 , scrollBarSize);
				bottomScroll.graphics.endFill();
				
				bottomScrollBar.addChild(bottomScroll);
				
				bottomScroll.addEventListener(MouseEvent.MOUSE_DOWN , onBotScrollMouseDown , false , 0 , true );
				bottomScroll.addEventListener(MouseEvent.MOUSE_UP , stopBotDrag , false , 0 , true);
				bottomScroll.addEventListener(MouseEvent.MOUSE_OUT, stopBotDrag , false , 0 , true);

			}
			
			if( contentHeight + scrollBarSize > editorSize ) 
			{
				rightScrollBar = new Sprite;
				rightScrollBar.x = editorSize - scrollBarSize;
				rightScrollBar.graphics.beginFill( 0xD3D3D3, 1 );
				rightScrollBar.graphics.drawRect(0, 0, scrollBarSize, editorSize);
				rightScrollBar.graphics.endFill();
				
				addChild(rightScrollBar);
				
				rightScroll = new Sprite();
				rightScroll.graphics.beginFill( 0xA0A0A0, 1 );
				rightScroll.graphics.drawRect(0, 0 , scrollBarSize, 50);
				rightScroll.graphics.endFill();
				
				rightScrollBar.addChild(rightScroll);
				
				rightScroll.addEventListener(MouseEvent.MOUSE_DOWN , onRightScrollMouseDown , false , 0 , true);
				rightScroll.addEventListener(MouseEvent.MOUSE_UP , stopRightDrag , false , 0 , true);
				rightScroll.addEventListener(MouseEvent.MOUSE_OUT, stopRightDrag , false , 0 , true);
			}
			
		}
		
		private function onRightScrollMouseDown( e : MouseEvent ) : void
		{
			var bound : Rectangle = new Rectangle(0,0,0,editorSize - 50);
			rightScroll.startDrag(false, bound);
			rightScroll.addEventListener(MouseEvent.MOUSE_MOVE , moveRightScroll ) ;
		}
		
		private function stopRightDrag( e : MouseEvent ) : void
		{
			rightScroll.removeEventListener(MouseEvent.MOUSE_MOVE , moveRightScroll ) ;
			rightScroll.stopDrag();
		}
		
		private function moveRightScroll ( e : MouseEvent ) : void
		{
			var oversize : int = (contentHeight + 15 ) - editorSize;
			
			var position : int = rightScroll.y;
			var maxpos : int = editorSize - 50;
			var ratio : Number = position / maxpos;
			
			
			tileContainer.y = -(oversize * ratio);
			
		}
		
		
		private function onBotScrollMouseDown( e : MouseEvent ) : void
		{
			var bound : Rectangle = new Rectangle(0,0,editorSize - 50,0);
			bottomScroll.startDrag(false, bound);
			bottomScroll.addEventListener(MouseEvent.MOUSE_MOVE , moveBottomScroll ) ;
		}
		
		private function moveBottomScroll ( e : MouseEvent ) : void
		{
			var oversize : int = (contentWidth + 15 )- editorSize ;
			
			var position : int = bottomScroll.x;
			var maxpos : int = editorSize - 50;
			var ratio : Number = position / maxpos;

			
			tileContainer.x = -(oversize * ratio);
			
		}
		
		private function stopBotDrag( e : MouseEvent ) : void
		{
			bottomScroll.removeEventListener(MouseEvent.MOUSE_MOVE , moveBottomScroll ) ;
			bottomScroll.stopDrag();
		}
		
		private function initConfigs() : void 
		{
			gridWidth = 30;
			gridHeight = 30;
			baseTime = 30000;
			setDefaultMode();
		}
		
		private function setDefaultMode() : void 
		{
			this.mode = PATH;
		}
	}
}