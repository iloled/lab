package labyrinth.editor
{
	import flash.display.Bitmap;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.utils.Timer;
	
	import labyrinth.level.event.WarpEvent;
	import labyrinth.utils.DisplayUtils;
	import labyrinth.utils.ImageUtils;
	import labyrinth.utils.PopupManager;

	public class EditorPopup
	{
		private var editor : LevelEditor;
		
		private var currentPopup : Sprite;
		private var popupName : String;
		private var jsonInput : TextField;
		private var timer : Timer;
		private static var LOAD_JSON : String = "addjson";
		private var currentPickedColor : uint = 0;
		
		public function EditorPopup(editor : LevelEditor)
		{
			this.editor = editor;
		}
		
		public function displayTimedPopup( message : String ) : void
		{
			removeCurrentPopup();
			currentPopup = PopupManager.createAndDisplayPopup(this.editor, 300,100);
			var title : TextField = DisplayUtils.createTextWithDefaultFormat(message);
			title.width = 250;
			title.x = 10
			title.y = 10;
			currentPopup.addChild(title);
			timer = new Timer(2000,1);
			timer.start();
			timer.addEventListener(TimerEvent.TIMER, function() : void { removeTimer() } );
			
		}
		
		private function removeTimer() : void
		{
			if( timer != null)
			{
				timer.stop();
				timer = null;
				
			}
			removeCurrentPopup();
		}
		
		public function displayKeyColorPopup() : void 
		{
			removeCurrentPopup();
			currentPopup = PopupManager.createAndDisplayPopup(this.editor, 200,200);
			
			
			var description : TextField = DisplayUtils.createTextWithDefaultFormat("Choose key color");
			description.width = 200;
			description.height = 50;
			description.x = 10
			description.y = 10;
			currentPopup.addChild(description);
			
			var container : Sprite = createColorPickerSprite();
			container.y = 30;
			currentPopup.addChild(container);
			
			var confirm : TextField = DisplayUtils.createTextWithDefaultFormat("Confirm ");
			confirm.x = 10
			confirm.y = 70;
			confirm.width = 200;
			confirm.height = 30;
			currentPopup.addChild(confirm);
			
			confirm.addEventListener(MouseEvent.CLICK, 
				function() : void 
				{ 
					editor.setKeyColor(this.currentPickedColor);
					removeCurrentPopup();
				} 
			);
		}
		
		public function displayStepPopup() : void 
		{
			removeCurrentPopup();
			currentPopup = PopupManager.createAndDisplayPopup(this.editor, 200,200);
			
			
			var newDoor : TextField = DisplayUtils.createTextWithDefaultFormat("Choose step color");
			newDoor.width = 200;
			newDoor.height = 50;
			newDoor.x = 10
			newDoor.y = 10;
			currentPopup.addChild(newDoor);
			
			var container : Sprite = createColorPickerSprite();
			container.y = 30;
			currentPopup.addChild(container);
			
			var confirm : TextField = DisplayUtils.createTextWithDefaultFormat("confirm");
			confirm.x = 10
			confirm.y = 70;
			confirm.width = 200;
			confirm.height = 30;
			currentPopup.addChild(confirm);
			
			confirm.addEventListener(MouseEvent.CLICK, 
				function() : void 
				{ 
					editor.setStepColor(this.currentPickedColor);
					removeCurrentPopup();
				} 
			);
		}
		
		public function displayGenerationPopup() : void 
		{
			removeCurrentPopup();
			currentPopup = PopupManager.createAndDisplayPopup(this.editor, 300,300);
			
			var title : TextField = DisplayUtils.createTextWithDefaultFormat("Choose generation algorithm");
			title.width = 200;
			title.height = 50;
			title.x = 10
			title.y = 10;
			currentPopup.addChild(title);
			
			var recursive : TextField = DisplayUtils.createTextWithDefaultFormat("Recursive backtracker");
			recursive.width = 200;
			recursive.height = 50;
			recursive.x = 10
			recursive.y = 40;
			currentPopup.addChild(recursive);
			recursive.addEventListener(MouseEvent.CLICK, 
				function() : void 
				{ 
					editor.generateRecursiveBacktracker();
					removeCurrentPopup();
				} 
			);
		}
		
		public function displayDoorPopup() : void 
		{
			removeCurrentPopup();
			currentPopup = PopupManager.createAndDisplayPopup(this.editor, 200,200);
			
			
			var newDoor : TextField = DisplayUtils.createTextWithDefaultFormat("Choose color");
			newDoor.width = 200;
			newDoor.height = 50;
			newDoor.x = 10
			newDoor.y = 10;
			currentPopup.addChild(newDoor);
			
			var container : Sprite = createColorPickerSprite();
			container.y = 30;
			currentPopup.addChild(container);
			
			var confirm : TextField = DisplayUtils.createTextWithDefaultFormat("New Door");
			confirm.x = 10
			confirm.y = 70;
			confirm.width = 200;
			confirm.height = 30;
			currentPopup.addChild(confirm);
			
			confirm.addEventListener(MouseEvent.CLICK, 
				function() : void 
				{ 
					editor.setDoorColor(this.currentPickedColor);
					removeCurrentPopup();
				} 
			);
				
			
		}
		
		
		private function createColorPickerSprite() : Sprite
		{
			var container : Sprite = new Sprite();
			
			var red : Sprite = new Sprite;
			DisplayUtils.fillRectSprite(red, 0xFF0000,20,20);
			red.x = 10; 
			red.y = 10;
			container.addChild(red);
			red.addEventListener(MouseEvent.CLICK, 
				function() : void { 
					DisplayUtils.drawBorder(red, 0xFFFFFF, 5); 
					this.currentPickedColor = 0xFF0000;
				}
				, false , 0, true);
			
			
			var green : Sprite = new Sprite;
			DisplayUtils.fillRectSprite(green, 0x00cc66,20,20);
			green.x = 40; 
			green.y = 10;
			container.addChild(green);
			green.addEventListener(MouseEvent.CLICK, 
				function() : void { 
					DisplayUtils.drawBorder(green, 0xFFFFFF, 5); 
					this.currentPickedColor = 0x00cc66;
				}
				, false , 0, true);
			
			var yellow : Sprite = new Sprite;
			DisplayUtils.fillRectSprite(yellow, 0xFFFF00,20,20);
			yellow.x = 70; 
			yellow.y = 10;
			container.addChild(yellow);
			yellow.addEventListener(MouseEvent.CLICK, 
				function() : void { 
					DisplayUtils.drawBorder(yellow, 0xFFFFFF, 5); 
					this.currentPickedColor = 0xFFFF00;
				}
				, false , 0, true);
			
			var orange : Sprite = new Sprite;
			DisplayUtils.fillRectSprite(orange,  0xff9900,20,20);
			orange.x = 100; 
			orange.y = 10;
			container.addChild(orange);
			orange.addEventListener(MouseEvent.CLICK, 
				function() : void { 
					DisplayUtils.drawBorder(orange, 0xFFFFFF, 5); 
					this.currentPickedColor = 0xff9900;
				}
				, false , 0, true);
			
			
			var blue : Sprite = new Sprite;
			DisplayUtils.fillRectSprite(blue,  0x3366ff,20,20);
			blue.x = 130; 
			blue.y = 10;
			container.addChild(blue);
			blue.addEventListener(MouseEvent.CLICK, 
				function() : void { 
					DisplayUtils.drawBorder(blue, 0xFFFFFF, 5); 
					this.currentPickedColor = 0x3366ff;
				}
				, false , 0, true);
			
			var violet : Sprite = new Sprite;
			DisplayUtils.fillRectSprite(violet,  0x6600cc,20,20);
			violet.x = 160; 
			violet.y = 10;
			container.addChild(violet);
			violet.addEventListener(MouseEvent.CLICK, 
				function() : void { 
					DisplayUtils.drawBorder(violet, 0xFFFFFF, 5); 
					this.currentPickedColor = 0x6600cc;
				}
				, false , 0, true);
			
			return container
		}
		
		public function displayWarpPopup( warpList : Array ) : void 
		{
			removeCurrentPopup();
			currentPopup = PopupManager.createAndDisplayPopup(this.editor, 300,500);
			var title : TextField = DisplayUtils.createTextWithDefaultFormat("Warp list");
			title.width = 200;
			title.height = 50;
			title.x = 10
			title.y = 10;
			currentPopup.addChild(title);
			
			var newWarp : TextField = DisplayUtils.createTextWithDefaultFormat("New warp");
			newWarp.width = 200;
			newWarp.height = 50;
			newWarp.x = 10
			newWarp.y = 50;
			currentPopup.addChild(newWarp);
			newWarp.addEventListener(MouseEvent.CLICK , function() : void { editor.addNewWarp();} ) ;
			var ypos : int = 100; 
			
			for ( var i : int = 0 ; i < warpList.length ; i++ ) 
			{
				var warp : WarpEvent = warpList[i];
				var sourceText : TextField = DisplayUtils.createTextWithDefaultFormat(warp.sourceX + " " + warp.sourceY);
				sourceText.width = 80;
				sourceText.height = 30;
				sourceText.y = ypos;
				sourceText.x = 10;
				currentPopup.addChild(sourceText);
				var cross : Bitmap = ImageUtils.getCrossIcon();
				var wrapper : Sprite = new Sprite;
				wrapper.x = 110;
				wrapper.y = ypos;
				wrapper.addChild(cross);
				wrapper.addEventListener(MouseEvent.CLICK, 
					function( e : MouseEvent ) : void 
					{ 
						//TODO  dirty but works : find warp index to delete based on wrapper y position
						var index : int = Math.round((e.target.y-100) / 50);
						editor.deleteWarp(index);
						removeCurrentPopup();
					} 
				) ;
				
				currentPopup.addChild(wrapper);
				ypos += 50;
			}
		}
		
		public function displayLoadJSONPopup() : void 
		{
				removeCurrentPopup();
				popupName = LOAD_JSON;
				currentPopup = PopupManager.createAndDisplayPopup(this.editor, 300,400);
				var title : TextField = DisplayUtils.createTextWithDefaultFormat("Load Level from JSON");
				var p : Sprite = currentPopup;
				title.width = 200;
				title.x = 10
				title.y = 10;
				p.addChild(title);
				
				var input : TextField = DisplayUtils.createTextInput( 200,200 , "");
				input.x = 10;
				input.y = 50;
				p.addChild(input);
				jsonInput = input;
				var button : Sprite = DisplayUtils.createButton("Load");
				button.x = 10;
				button.y = 280;
				p.addChild(button);
				
				button.addEventListener(MouseEvent.CLICK, 
					function() : void { 
						editor.loadJSON(jsonInput.text);
						removeCurrentPopup();
					} 
				);
		}
		
		private function removeCurrentPopup() : void 
		{
			if ( currentPopup != null )
			{
				this.editor.removeChild(currentPopup);
			}
			
			currentPopup = null;
				
		}
	}
}