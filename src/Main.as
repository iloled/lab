package
{
	import caurina.transitions.Tweener;
	
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.external.ExternalInterface;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.filters.GradientBevelFilter;
	import flash.filters.GradientGlowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	import flash.utils.Timer;
	
	import labyrinth.controls.Control;
	import labyrinth.editor.LevelEditor;
	import labyrinth.events.GameEvent;
	import labyrinth.font.FontUtils;
	import labyrinth.level.Cell;
	import labyrinth.level.Grid;
	import labyrinth.level.Level;
	import labyrinth.level.Player;
	import labyrinth.level.Tile;
	import labyrinth.level.data.LevelGenerator;
	import labyrinth.level.data.Tracking;
	import labyrinth.level.effect.WarpEffect;
	import labyrinth.level.popup.GamePopup;
	import labyrinth.mainscreen.MainMenu;
	import labyrinth.utils.DisplayUtils;
	import labyrinth.utils.ImageUtils;
	import labyrinth.utils.MathUtils;
	
	
	// TODO check : find new image for clock
	// TODO : optimize doors json
	
	[SWF(width="700", height="500", backgroundColor="#000000", frameRate="60")]
	public class Main extends Sprite
	{
		public var player : Player;
		public var grid : Grid;
		
		public var levelMask : Sprite; 
		
		public var container : Sprite;
		public var containerMask : Sprite;
		
		// CONTAINERS
		
		public var inventoryContainer : Sprite;
		
		// CONFIGS 
		
		public var paddingTop : int = 50;
		public var paddingLeft : int = 50;
		private var ms : int = 0;
		private var second : int = 60;
		
		// GAME 
		
		public var control :Control;
		
		// DISPLAY
		
		private var startMessage : Sprite;
		
		
		// LEVELS
		
		public var levels : Array = [];
		public static const LEVEL_NUMBER : int = 2;
		public var gameScreenSize : int = 400;			
		
		private var currentLevel : Level;
		private var currentLevelIndex : int = 0;
		
		// EDITOR
		
		private var levelEditor : LevelEditor;
		
		// game data
		
		private var extraTime : uint = 0;
		private var retries : uint = 0;
		
		
		public function Main()
		{
			// load font
			
			var f : FontUtils;
			var t : LevelGenerator;
			var c : Cell;
			
			stage.scaleMode = StageScaleMode.NO_SCALE;
			//StageScaleMode.EXACT_FIT
			// stage background
			
			stage.frameRate = 60;
			
			// gameListener
			
			stage.addEventListener(GameEvent.STAGE_CLEAR , onStageClear);
			stage.addEventListener(GameEvent.TIME_OUT , timeOut);
			stage.addEventListener(GameEvent.GAME_START, function() : void { startGame();}); 
			// TODO : absolutely dirty
			stage.addEventListener(GameEvent.CENTER_NEEDED, function() : void { centerCurrentLevel() });
			
			
			//displayGameEnd();
			//initMainMenu();
			//initGame();
			initLevelEditor();
		}
		
		private function initGameScreenContainer() : void 
		{
			this.container = new Sprite;
			
			this.container.x = paddingLeft;
			this.container.y = paddingTop;
			container.graphics.beginFill( 0x000000, 1 );
			container.graphics.drawRect(0, 0,gameScreenSize, gameScreenSize);
			container.graphics.endFill();
			addChild(this.container);
			
			// game screen mask
			
			containerMask = new Sprite;
			containerMask.graphics.clear();
			containerMask.graphics.beginFill( 0x000000, 1 );
			containerMask.graphics.drawRect(0, 0, gameScreenSize, gameScreenSize);
			containerMask.graphics.endFill();
			container.mask = containerMask;
			
			container.addChild(containerMask);
			
		}
		
		private function initMainMenu() : void
		{
			addChild(new MainMenu);
		}
		
		public function startGame() : void
		{
			Tracking.trackStart();
			
			resetScore();	
			this.removeChildren();
			initGame();
			
		}
		
		private function resetScore() : void 
		{
			extraTime = 0;
			retries = 0;
		}
		
		private function initLevelEditor() : void
		{
			levelEditor  = new LevelEditor();
			// TODO : for some reason cant change y
			//levelEditor.y = paddingTop;
			addChild(levelEditor);
		}
		
		private function initGame() : void 
		{
			// game screen
			
			initGameScreenContainer();
			
		
			// container center point
			
			var centerX : Number = containerMask.width / 2;
			var centerY : Number = containerMask.height / 2;
			
			
			// game screen border 
			
			var border  = new MovieClip;
			border.x = 0
			border.y = 0;
			border.graphics.clear();
			border.graphics.lineStyle(5, 0xffffff);
			border.graphics.drawRect(0, 0, gameScreenSize, gameScreenSize);
			border.graphics.endFill();
			
			container.addChild(border);
			
			
			// Inventory container
			
			inventoryContainer = new Sprite;
			
			var text : TextField = DisplayUtils.createTextWithDefaultFormat("Inventory",16);
			
			text.x = 10;
			text.y = 10;
			
			inventoryContainer.addChild(text);
			inventoryContainer.x = this.gameScreenSize + paddingLeft *2 ;
			
			inventoryContainer.graphics.clear();
			inventoryContainer.graphics.lineStyle(2, 0xffffff);
			inventoryContainer.graphics.drawRect(0, 0, 100, gameScreenSize);
			inventoryContainer.graphics.endFill();
			inventoryContainer.y = paddingTop;
			
			addChild( inventoryContainer);
			
			
			// player and grid
			
			initLevels();
			
			// TODO temporary test
			
			currentLevel = levels[currentLevelIndex];
			
			displayCurrentLevelMessage();
			
			//startCurrentLevel();
			
			// temp junk
			
			
			
			//displayDungeonClearMessage();
		}
		
		
		
		private function displayCurrentLevelMessage() : void
		{
			
			var g : GamePopup;
			var popup : Sprite = GamePopup.createPopup(stage , 200,200,0x000000);
			startMessage = popup;
			
			var currentLevel : Level = levels[this.currentLevelIndex];
			
			var levelName : TextField;
			var message : TextField ;
			
			if( currentLevel.levelName != null) 
			{
				levelName  = 
					DisplayUtils.createTextWithDefaultFormat(currentLevel.levelName, 25, true);
			}
			else
			{
				levelName  = 
					DisplayUtils.createTextWithDefaultFormat( "STAGE "+(currentLevelIndex+1), 25 , true);
			}
			levelName.x = 25;
			levelName.y = 20;
			levelName.width =150;
			popup.addChild(levelName);
			
			if( currentLevel.message != null )
			{
				 message  = 
					DisplayUtils.createTextWithDefaultFormat(currentLevel.message, 20, true);
				 message.x = 25;
				 message.width = 150;
				 message.height = 100;
				 message.y = 60;
				 message.multiline = true;
				 message.wordWrap = true;
				 
				 popup.addChild(message);
				 
			}
			
			
			
			var confirm : Sprite = DisplayUtils.createButton( "START", 0xFFFFFF, 0x000000, 80, 30,25 );
			confirm.x = 60;
			confirm.y = 150;
			popup.addChild(confirm);
			confirm.addEventListener(MouseEvent.CLICK , 
				function() : void 
				{
					startCurrentLevel();
				}
			);
			
			centerPopup(popup);
			this.blurGameScreen();
		}
		
		private var messageSprite : Sprite;
		
		public function displayTimeOutMessage()
		{
			var sprite : Sprite = new Sprite();
			DisplayUtils.fillRectSprite(sprite, 0x000000 , 200,200);
			DisplayUtils.drawBorder(sprite,0xffffff,5);
			
			//===
			
			centerPopup(sprite);
			
			var text : TextField = DisplayUtils.createTextWithFont("GAME OVER\n TIME OUT", "square", 25 );
			text.width = 200;
			sprite.addChild(text);
			text.x = 20;
			text.y = 10;
			
			var restartButton:Sprite = new Sprite();
			DisplayUtils.fillRectSprite(restartButton, 0xffffff , 125,30);
			
			var buttonText  : TextField = DisplayUtils.createTextWithFont("RESTART", "square", 25 , 0x000000);
			buttonText.width = 125;
			restartButton.addChild(buttonText);
			
			restartButton.x = 30
			restartButton.y = 100;
			
			sprite.addChild(restartButton);
			
			restartButton.addEventListener(MouseEvent.CLICK, onStageRestart, false , 0 , true );
			this.addChild(sprite);
			messageSprite = sprite;
			
			this.blurGameScreen();
		}
		
		public function displayGameEnd() : void
		{
			initGameScreenContainer();
			var score : int = 0 ;
			var sprite : Sprite = new Sprite();
			DisplayUtils.fillRectSprite(sprite, 0x000000 , 400,300);
			DisplayUtils.drawBorder(sprite,0xffffff,5);
			
			//===
			
			centerPopup(sprite);
			
			var text : TextField = DisplayUtils.createTextWithFont("FINAL SCORE", "square", 25 );
			text.width = 200;
			sprite.addChild(text);
			text.x = 20;
			text.y = 10;
			
			var time : TextField = DisplayUtils.createTextWithFont("BONUS TIME : " + this.extraTime + " sec", "square", 20);
			time.width = 200;
			sprite.addChild(time);
			time.x = 20;
			time.y = 40;
			
			var timeScore : uint = extraTime * 10;
			var timeScoreLabel : String = "" + timeScore;
			if( timeScore > 0 )
			timeScoreLabel = "+"+timeScoreLabel;
			var timeScoreText : TextField = DisplayUtils.createTextWithFont("" + timeScoreLabel, "square", 20 );
			timeScoreText.width = 200;
			sprite.addChild(timeScoreText);
			timeScoreText.x = 220;
			timeScoreText.y = 40;
			
			var retryScore : int = retries * -50; 
			var retries : TextField = DisplayUtils.createTextWithFont("Retry : " + this.retries, "square", 20);
			retries.width = 180;
			sprite.addChild(retries);
			retries.x = 20;
			retries.y = 70;
			
			if( this.retries > 0)
			{
				var retryText : TextField = DisplayUtils.createTextWithFont("" + retryScore, "square", 20 );
				retryText.width = 200;
				sprite.addChild(retryText);
				retryText.x = 220;
				retryText.y = 70;
			}
			
			
			
			
			
			var totalScore : int = timeScore + retryScore;
			Tracking.trackEndGame(this.retries, this.extraTime, totalScore);
			var total : TextField = DisplayUtils.createTextWithFont("Score : ", "square", 20);
			total.width = 180;
			sprite.addChild(total);
			total.x = 20;
			total.y = 100;
			
			var scoreText : TextField = DisplayUtils.createTextWithFont("" + totalScore, "square", 20);
			scoreText.width = 100;
			sprite.addChild(scoreText);
			scoreText.x = 220;
			scoreText.y = 100;
			
			var thanks : TextField = DisplayUtils.createTextWithFont("THANK YOU FOR PLAYING!", "square", 25 );
			thanks.width = 300;
			sprite.addChild(thanks);
			
			thanks.x = 20
			thanks.y = 150;
			
			var restartButton:Sprite = new Sprite();
			DisplayUtils.fillRectSprite(restartButton, 0xffffff , 200,30);
			var buttonText  : TextField = DisplayUtils.createTextWithFont("GO TO MAIN MENU", "square", 25 , 0x000000);
			buttonText.width = 250;
			restartButton.addChild(buttonText);
			
			restartButton.x = 20
			restartButton.y = 200;
			
			sprite.addChild(restartButton);
			
			restartButton.addEventListener(MouseEvent.CLICK, function() : void { goToMain(); }, false , 0 , true );
			this.addChild(sprite);
			messageSprite = sprite;
			
			if( container != null )
				this.blurGameScreen();
			
		}
		
		public function displayDungeonClearMessage() : void
		{
			currentLevel.stop();
			var sprite : Sprite =GamePopup.createPopup(stage , 200,200,0x000000);
			
			
			centerPopup(sprite);
			
			var text : TextField = DisplayUtils.createTextWithFont("STAGE CLEAR", "square", 25 );
			text.width = 200;
			sprite.addChild(text);
			text.x = 20;
			text.y = 10;
			
			var remainingTime : int = currentLevel.getRemainingSeconds();
			
			extraTime += remainingTime;
			
			var timeLabel : TextField = 
				DisplayUtils.createTextWithFont("remaining time :\n"+remainingTime + " s" , "square", 14 );
			timeLabel.y = 50;
			timeLabel.x = 10;
			
			sprite.addChild(timeLabel);
				
			var nextButton:Sprite = new Sprite();
			DisplayUtils.fillRectSprite(nextButton, 0xffffff , 100,30);
			
			var buttonText  : TextField = DisplayUtils.createTextWithFont("NEXT", "square", 25 , 0x000000 );
			nextButton.addChild(buttonText);
			
			nextButton.x = 100
			nextButton.y = 100;
			
			
			sprite.addChild(nextButton);
			
			nextButton.addEventListener(MouseEvent.CLICK, nextStage, false , 0 , true );
			
			this.addChild(sprite);
			messageSprite = sprite;
			
			this.blurGameScreen();
			
		}
		
		private function centerPopupInContainer(s : Sprite) : void
		{
			s.x = (container.mask.width - s.width) / 2 ;
			s.y = (container.mask.height - s.height) / 2 ;
		}
		
		private function centerPopup( s : Sprite ) : void 
		{
			s.x = (700 - s.width) / 2 ;
			s.y = (500 - s.height) / 2 ;
		}
		
	
		
		// TODO inventory container should be somewhere else;
		private function blurGameScreen() : void 
		{
			DisplayUtils.blurDisplayObject(container);
			if( inventoryContainer != null)
				DisplayUtils.blurDisplayObject(inventoryContainer);
		}
		
		private function unblurGameScreen() : void 
		{
			container.filters = [];
			if( inventoryContainer != null)
				inventoryContainer.filters = [];
		}
		
	
		private var destX : int;
		private var destY : int;
		private var levelSprite : Sprite;
		
		private function centerLevelSprite( sprite : Sprite , player : Player ) : void
		{
			levelSprite = sprite;
			container.addChild(sprite);
			destX = gameScreenSize / 2;
			destX -= ( player.x ) * Tile.TILE_SIZE + Tile.TILE_SIZE /2;
			
			destY = gameScreenSize / 2;
			destY -= ( player.y ) * Tile.TILE_SIZE + Tile.TILE_SIZE /2;

			
		
			Tweener.addTween(sprite, {x:destX, 
				y:destY, onComplete : warpEnd,
				time:1,
				transition:"easeOutCubic"});
			disableControls();

			
			/*sprite.x = destX;
			sprite.y = destY;

		
			
			// start warp animation
			/*
			var warpTimer : Timer = new Timer(25,60);
			warpTimer.addEventListener(TimerEvent.TIMER, moveAnimation);
			warpTimer.addEventListener(TimerEvent.TIMER_COMPLETE, warpEnd);
			
			warpTimer.start();*/
		}
	
		

		
		private function warpEnd() : void
		{
			levelSprite.x = destX;
			levelSprite.y = destY;
			currentLevel.getPlayer().show();
			currentLevel.getPlayer().draw();
			enableControls();
			
		}
		
		// generate random levels
		// TODO should be somewhere else
		private function initLevels() : void
		{
			levels = new Array();

			var t : LevelGenerator;
			levels = LevelGenerator.getJSONLevels(stage);

			/*for ( var i : int = 0 ; i < LEVEL_NUMBER; i++ ) 
			{
				var level : Level = new Level( 
					MathUtils.getRandomNumberInRange(5,20),
					MathUtils.getRandomNumberInRange(5,20),
					stage) ;
				level.initRandomLevel();
				levels.push(level);
			}*/
		}
		
		private function goToMain() : void 
		{
			this.unblurGameScreen();
			this.removeChildren();
			this.initMainMenu();
		}
		
		private function unloadCurrentLevel() : void
		{
			container.removeChild(currentLevel.getSprite());
			control.removeListener();
			resetInventory();
			this.removeChild(currentLevel.getTimerText());
		}
		
		private function resetInventory() : void
		{
			// inventory is 2nd element
			inventoryContainer.removeChildAt(1);
		}
		
		private function startCurrentLevel() : void
		{
			Tracking.trackPlay(currentLevelIndex+1);
			removeMessage();
			this.unblurGameScreen();
			control = new Control(this.stage, currentLevel);
			control.init();
			
			
			stage.stageFocusRect = false;
			stage.focus = container;
			currentLevel.start();
			this.addChild(currentLevel.getTimerText());
			inventoryContainer.addChild(currentLevel.getInventoryDisplay());
			centerLevelSprite(currentLevel.getSprite(), currentLevel.getPlayer());
			
		}
		
		private function removeMessage() : void
		{
			stage.removeChild(startMessage);
			startMessage = null;
		}
		
		private function restartStage() : void
		{
			currentLevel.reset();
			Tracking.trackRetry(currentLevelIndex+1);
			
			//control = new Control(this.stage, currentLevel);
			//control.init();
			currentLevel.start();
			centerCurrentLevel();
			stage.stageFocusRect = false;
			stage.focus = container;
			
			
		}
		
		private function centerCurrentLevel():  void
		{
			centerLevelSprite(currentLevel.getSprite(), currentLevel.getPlayer());
		}
		
		
		private function disableControls() : void
		{
			control.enabled = false;
		}
		
		private function enableControls() : void
		{
			control.enabled = true;
		}
		
		
		// event handler
		
		private function onStageClear( e : Event ) : void 
		{
			Tracking.trackComplete(this.currentLevelIndex+1, currentLevel.retries, currentLevel.getRemainingSeconds()); 
			displayDungeonClearMessage();
			disableControls();
		}
		
		
		
		private function timeOut( e : Event ) : void
		{
			displayTimeOutMessage();
			disableControls();
		}
		
		private function onStageRestart(e : Event ) : void
		{
			retries ++;
			
			removeChild(messageSprite);
			messageSprite = null;
			restartStage();
			unblurGameScreen();
		}
		
		private function nextStage( e : Event ) : void 
		{
			unloadCurrentLevel();
			removeChild(messageSprite);
			messageSprite = null;
			
			if( isLastStage() ) 
			{
				displayGameEnd();
			}
			else
			{
				currentLevelIndex ++;
				currentLevel = levels[currentLevelIndex];
				displayCurrentLevelMessage();
			}
			
		}
		
		private function isLastStage() : Boolean 
		{
			return currentLevelIndex == levels.length - 1;
		}
		
		
	}
}